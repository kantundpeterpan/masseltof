#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 13:02:34 2019

@author: kantundpeterpan
"""

import networkx as nx
from MSanalyzer_class import *
from Species.Species_network_x import SpeciesX


species_str = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-DAP(-Ala-C_ter*Ala)-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'

#species_str = 'N_ter*Ala-Glu-C_ter*Lys'
test = SpeciesX(species_str, MSanalyzer(filename=''))
