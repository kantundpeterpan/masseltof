#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:42:00 2019

@author: kantundpeterpan
"""

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt

class peak_label_dialog(QtWidgets.QDialog):
    
    def __init__(self, parent = None, analysis = None):
        QtWidgets.QDialog.__init__(self)
        
        self.analysis = analysis
        
        self.deconvolute_txt_changed = lambda: self.deconvolute(self.z_box.text(),
                                                                self.m_z_box.text(),
                                                                self.h_box.text(),
                                                                self.na_box.text(),
                                                                self.k_box.text())
        
        self.vbox = QtWidgets.QFormLayout(self)

        self.peak_label_box = QtWidgets.QLineEdit(parent = self)
        
        self.z_box = QtWidgets.QLineEdit(parent = self)
        self.z_box.textChanged.connect(self.deconvolute_txt_changed)
        
        self.m_z_box = QtWidgets.QLineEdit(parent = self)
        self.m_z_box.setReadOnly(True)
        self.m_z_box.textChanged.connect(self.deconvolute_txt_changed)
        self.m_z_box.textChanged.connect(self.calc_ppm)
        
        #mobs!
        self.mass_box = QtWidgets.QLineEdit(parent = self)
        self.mass_box.setReadOnly(True)
        self.mass_box.textChanged.connect(self.deconvolute_txt_changed)
        self.mass_box.textChanged.connect(self.calc_ppm)
        
        #counter_ions
        self.h_box_label = QtWidgets.QLabel()
        self.h_box_label.setText(r'H⁺')
        self.h_box = QtWidgets.QLineEdit(parent = self)
        self.h_box.textChanged.connect(self.deconvolute_txt_changed)
        
        self.na_box_label = QtWidgets.QLabel()
        self.na_box_label.setText('Na⁺')
        self.na_box = QtWidgets.QLineEdit(parent = self)
        self.na_box.textChanged.connect(self.deconvolute_txt_changed)
        
        self.k_box_label = QtWidgets.QLabel()
        self.k_box_label.setText('K⁺')
        self.k_box = QtWidgets.QLineEdit(parent = self)
        self.k_box.textChanged.connect(self.deconvolute_txt_changed)
        
        #mcalc!!
        self.mass_calc_line = QtWidgets.QGroupBox('Calculated Mass')
        layout = QtWidgets.QGridLayout()
        layout.setColumnStretch(0,2)
        layout.setColumnStretch(1,2)
        #sDropdown Menu for selection of a molecule from the mol_db
        self.mass_calc_drop_menu = QtWidgets.QComboBox(self)
        for mol in self.analysis.msanalyzer.mol_db.index.values:
            self.mass_calc_drop_menu.addItem(mol)
        self.mass_calc_drop_menu.activated.connect(self.load_mass_calc)
        self.mass_calc_drop_menu.activated.connect(self.populate_labeling_drop_menu)
        self.mass_calc_drop_menu.currentIndexChanged.connect(self.load_mass_calc)
        self.mass_calc_drop_menu.currentIndexChanged.connect(self.populate_labeling_drop_menu)
        layout.addWidget(self.mass_calc_drop_menu, 0, 0)
        
        #Dropdown Menu to select labeling variant
        self.labeling_variant_drop_menu = QtWidgets.QComboBox(self)
        self.labeling_variant_drop_menu.activated.connect(self.load_mass_calc)
        self.labeling_variant_drop_menu.currentIndexChanged.connect(self.load_mass_calc)
        
        layout.addWidget(self.labeling_variant_drop_menu, 0, 1)
        self.mass_calc_line.setLayout(layout)
        
        
        #Widget for showing the calculated mass, retrieved from mol_db
        self.mass_calc_box = QtWidgets.QLineEdit(self)
        self.mass_calc_box.setReadOnly(True)
        self.mass_calc_box.textChanged.connect(self.calc_ppm)
        
        #Widget for showing delta m in ppm
        self.delta_ppm_box = QtWidgets.QLineEdit(self)
        self.delta_ppm_box.setReadOnly(True)
        
        self.remarks_box = QtWidgets.QPlainTextEdit(parent = self)
        
        self.button_ok = QtWidgets.QPushButton('OK', parent = self)
        self.button_ok.clicked.connect(self.set_content)
        
        self.button_cancel = QtWidgets.QPushButton('Cancel', parent = self)
        self.button_cancel.clicked.connect(self.reject)
        self.rejected.connect(self.reset_content)
        
        self.vbox.addRow('peak label', self.peak_label_box)
        self.vbox.addRow('z', self.z_box)
        self.vbox.addRow(r'm/z', self.m_z_box)
                       
        self.counter_ions_line = QtWidgets.QGroupBox('Counter Ions')
        
        layout = QtWidgets.QGridLayout()
        layout.setColumnStretch(0,6)
        layout.addWidget(self.h_box_label, 0, 0)
        layout.addWidget(self.h_box, 0, 1)
        layout.addWidget(self.na_box_label, 0, 2)
        layout.addWidget(self.na_box, 0, 3)
        layout.addWidget(self.k_box_label, 0, 4)
        layout.addWidget(self.k_box, 0, 5)
        
        self.counter_ions_line.setLayout(layout)
        
        self.vbox.addRow(self.counter_ions_line)
        
        self.vbox.addRow('mass', self.mass_box)
        
        self.vbox.addRow(self.mass_calc_line)
        self.vbox.addRow('mass calc', self.mass_calc_box)
        self.vbox.addRow('delta m', self.delta_ppm_box)
        
        self.vbox.addRow('remarks', self.remarks_box)
        self.vbox.addRow(self.button_ok, self.button_cancel)
        
        self.title = 'Peak labeling'
        self.left = 10
        self.top = 10
        self.width = 300
        self.height = 420
               
    def initUI(self, m_z = '', z = ''):
        
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
       #add possibility for counter ion selection       
        self.h_box.setText(str(z))
        self.na_box.setText('0')
        self.k_box.setText('0')
        
        z = int(z)
        m_z = float(m_z)
       
        if not float(m_z) in self.analysis.msanalyzer.data.peaks.index.values:
            self.z_box.setText(str(z))
            self.m_z_box.setText(str(m_z))
                       
            mass = round((m_z*z)-z*1.007276466583,4)        
            
            self.mass_box.setText(str(mass))
            
        else:

            data = self.analysis.msanalyzer.data.peaks.loc[m_z]
            
            label = data.peak_label
            z = data.z
            mass = data.mass
            remarks = data.remarks
            
            self.peak_label_box.setText(str(label))
            self.z_box.setText(str(z))
            self.m_z_box.setText(str(m_z))
            self.mass_box.setText(str(mass))
            self.remarks_box.setPlainText(remarks)
            
        self.mass_calc_drop_menu.setCurrentIndex(0)
        self.show()
        
    def reset_content(self):
        self.peak_label_box.setText('')
        self.z_box.setText('')
        self.m_z_box.setText('')
        self.mass_box.setText('')
        self.remarks_box.setPlainText('')
        
    def populate_labeling_drop_menu(self):
        self.labeling_variant_drop_menu.clear()
        species = self.mass_calc_drop_menu.currentText()
        variants = self.analysis.msanalyzer.mol_db.loc[species].species_obj.labeling_variants
        for variant in variants.index.values:
            self.labeling_variant_drop_menu.addItem(variant)
        self.labeling_variant_drop_menu.setCurrentIndex(0)
        
    def deconvolute(self, z, m_z, H=None, Na = 0, K = 0):
        try:
            z = int(z)
            m_z = float(m_z)
            H = int(H)
            Na = int(Na)
            K = int(K)
        except:
            return
        
        if H == None:
            H = z 
        
        mass = self.analysis.deconvolute(z, m_z, H, Na, K)
        
        mass = round(mass, 4) 
        
        self.mass_box.setText(str(mass))
    
    def load_mass_calc(self):
        
        species = self.mass_calc_drop_menu.currentText()
        labeling = self.labeling_variant_drop_menu.currentText()
        
        if not labeling == '':
            variants = self.analysis.msanalyzer.mol_db.loc[species].species_obj.labeling_variants
            mass_calc = variants.loc[labeling].monoisotopic_mass
            
            self.mass_calc_box.setText(str(mass_calc))
            
        else:
            return
    
    def calc_ppm(self):
        try:
            m_obs = float(self.mass_box.text())
            m_calc = float(self.mass_calc_box.text())
        except:
            return
        
        ppm = ((m_calc - m_obs)/m_calc) * 1000000
        
        self.delta_ppm_box.setText(str(ppm))
    
    def set_content(self):
        
        label = self.peak_label_box.text()
        m_z = float(self.m_z_box.text())
        z = int(float(self.z_box.text()))
        deconv = float(self.mass_box.text())
        calc_mass = float(self.mass_calc_box.text())
        delta = float(self.delta_ppm_box.text())
        
        remarks = self.remarks_box.toPlainText()
                
        if not m_z in self.analysis.msanalyzer.data.peaks.index:
            self.analysis.annotate_peak(label,
                                        m_z,
                                        z,
                                        deconv,
                                        calc_mass,
                                        delta,
                                        remarks)
            
        else:
            self.analysis.update_annotation(m_z, label, z, remarks)
            
        self.reset_content()
        
        self.accept()