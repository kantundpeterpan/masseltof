#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:28:52 2019

@author: kantundpeterpan
"""
import IPython

ipython = None#IPython.get_ipython()

if ipython:
    ipython.run_line_magic('gui','qt')
    print('set gui')
    
from .IsoDB import IsoDB
IsoDB.isotopize()
   
from .MSanalyzer_class import MSanalyzer
try:
    MSanalyzer.add_species_obj()
except:
    pass

from . import MS2Helper
from . import PeakTools
from . import PlotTools
from . import Species


