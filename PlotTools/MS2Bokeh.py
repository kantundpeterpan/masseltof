#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 18:26:28 2020

@author: kantundpeterpan
"""

from itertools import cycle

from bokeh.palettes import Category10
from bokeh.plotting import Figure

from bokeh.models import ColumnDataSource, TableColumn, HoverTool
from bokeh.models.widgets import Tabs, Panel, DataTable
from bokeh.models.annotations import LabelSet
from bokeh.layouts import Row, Column

from copy import deepcopy

import sys
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')

from MassAnalyzer.PlotTools import NeutralLossPlotter as NP

class MS2Bokeh():
    
    colors = cycle(Category10[10][1:])
    cols_to_keep = ['mz_obs', 'mz_calc', 'ppm', 'frags']
    
    def __init__(self, ms2, neutral_loss = None, label = True,
                 xlims = (45, 1000)):
        
        self.ms2search = deepcopy(ms2)
        self.createPlot(neutral_loss=neutral_loss,
                        label = label,
                        xlims = xlims)
        self.p.legend.click_policy = 'hide'
        self.createPeakTables()
        self.layout = Column(children=[self.upper_row_tab, self.p],
                             sizing_mode = 'stretch_both')
    
    def createPeakTables(self):
        
        #should return the row
        
        #if unique + ambig
        ##row with two table
        ### ambig
        if hasattr(self.ms2search, 'found_ambig'):
            
            source = ColumnDataSource(self.ms2search.found_ambig[self.cols_to_keep+['group']].round(4))
            columns = [TableColumn(field=c, title=c) for c in self.ms2search.found_ambig[self.cols_to_keep+['group']].columns]
            
            table = DataTable(source = source, columns = columns)
            table.sizing_mode = 'stretch_both'
            
            ambig_tab = Tabs(tabs = [Panel(child = table, title = 'common')])
            ambig_tab.sizing_mode = 'stretch_both'
            
            sources = [[df.id.unique()[0],
                        ColumnDataSource(df[self.cols_to_keep].round(4)),
                        [TableColumn(field=c, title=c) for c in df[self.cols_to_keep].columns]] 
                       for df in (*self.ms2search.found_unique,) if not df.empty]
            
            unique_tab = Tabs(tabs = [Panel(child=DataTable(source=s, columns = cols),
                                             title=it) for it,s,cols in sources])
            for tab in unique_tab.tabs:
                tab.child.sizing_mode = 'stretch_both'
                
            unique_tab.sizing_mode = 'stretch_both'
             
            self.upper_row_tab = Row(children=[ambig_tab, unique_tab])
            self.upper_row_tab.sizing_mode = 'stretch_both'
             
            return

        
        #if only found
        ##single table
        if hasattr(self.ms2search, 'found'):
        
            source = ColumnDataSource(self.ms2search.found[self.cols_to_keep].round(4))
            columns = [TableColumn(field=c, title=c) for c in self.ms2search.found[self.cols_to_keep].columns]
            
            table = DataTable(source = source, columns = columns)
            table.sizing_mode = 'stretch_both'
            
            found_tab = Tabs(tabs = [Panel(child = table, title = 'matched')])
            found_tab.sizing_mode = 'stretch_both'
            
            self.upper_row_tab = Row(children=[found_tab])
            self.upper_row_tab.sizing_mode = 'stretch_both'
             
            return
            
    def createPlot(self, neutral_loss = None,
                   label = True,
                   xlims = (45, 1000)):
        self.hovert = HoverTool(
                tooltips=[
                        ('mz', '@mz_obs{0.00}'),
                        ('group', '@group'),
                        ('frag', '@frags')
                    ],
                names = []
            )
        
        self.p = Figure(sizing_mode = 'stretch_both',
                        tools = ['box_zoom', 
                                    'undo',
                                    'redo',
                                    'reset',
                                    self.hovert,
                                    'save'],
                        x_range = xlims,
                        y_range = (-0.05*self.ms2search.msanalyzer.data.raw.y.max(),
                                   1.25*self.ms2search.msanalyzer.data.raw.y.max())
                        )
        
        self.p.output_backend = 'svg'
        
        self.p.xaxis.axis_label = 'm/z'
        self.p.yaxis.axis_label = 'Intensity (a.u.)'
        
        self.p.line(self.ms2search.msanalyzer.data.raw.x,
               self.ms2search.msanalyzer.data.raw.y)
    
        if hasattr(self.ms2search, 'found_ambig'): 
            
            df = self.ms2search.found_ambig
            df['peak_no'] = list(range(df.shape[0]))
            df['peak_no'] = df['peak_no'] + 1
            df['peak_label'] = df.mz_obs.round(3)
            print(df.frags.head())
            df.frags  = df.frags.apply(lambda x: ' | '.join(['-'.join(b[0]) for b in x])).astype(str)
            print(df.frags.head())
            source = ColumnDataSource(df)
            
            self.p.scatter(source = source, x='mz_obs', y='y', size=7,
                           color = Category10[10][0],
                           legend_label = 'common',
                           name = 'common')
            
            if label:
                labels = LabelSet(x = 'mz_obs', y = 'y', 
                          text = 'peak_label', 
                          x_offset = 4, y_offset = 9,
                          text_color = Category10[10][0],
                          source = source, angle = 1.57,
                          text_font_size = '11pt',
                          render_mode = 'canvas')
            
                self.p.add_layout(labels)
            
            self.hovert.names.append('common')
            
            if neutral_loss:
                for l in neutral_loss:
                    NP(l).plotNeutralLoss(
                            self.ms2search.found_ambig,
                            self.ms2search.msanalyzer.data.raw,
                            self.p,
                            line_color = 'grey',
                            legend_label = 'loss %s common' % l,
                            line_dash = 'dotted'
                        )
            
        if hasattr(self.ms2search, 'found_unique'):
            for i,df in enumerate(self.ms2search.found_unique,1):
                if not df.empty:
                    
                    name = df.id.unique()[0]
                    df['peak_no'] = list(range(df.shape[0]))
                    df['peak_no'] = df['peak_no'] + 1
                    df['peak_label'] = df.mz_obs.round(3)
                    df.frags  = df.frags.apply(lambda x: ' | '.join(['-'.join(b) for b in x]))
                    
                    source = ColumnDataSource(df)
                    color = next(self.colors)
                    
                    self.p.scatter(source = source,
                                   x = 'mz_obs', y = 'y',
                               color = color,#self.colors[i],
                               legend_label = name,
                               name = name,
                               size=10)
                    if label:
                        labels = LabelSet(x = 'mz_obs', y = 'y', 
                                  text = 'peak_label', 
                                  x_offset = 4, y_offset = 9,
                                  text_color = color,#self.colors[i],
                                  source = source, angle = 1.57,
                                  text_font_size = '11pt',
                                  render_mode = 'canvas')
                
                        self.p.add_layout(labels)
                            
                    self.hovert.names.append(name)
                    
                    if neutral_loss:
                        for l in neutral_loss:
                            NP(l).plotNeutralLoss(
                                    df,
                                    self.ms2search.msanalyzer.data.raw,
                                    self.p,
                                    line_color = 'grey',
                                    legend_label = 'loss %s %s' % (l, name),
                                    line_dash = 'dotted'
                            )
                    
        if hasattr(self.ms2search, 'found'):
            
            df = self.ms2search.found
            df['peak_no'] = list(range(df.shape[0]))
            df['peak_no'] = df['peak_no'] + 1
            df['peak_label'] = df.mz_obs.round(3)
            df.frags  = df.frags.apply(lambda x: ' | '.join(['-'.join(b) for b in x]))
            
            source = ColumnDataSource(df)
            
            self.p.scatter(source = source,
                       x='mz_obs', y='y', size=10,
                       color = Category10[10][1],
                       name = 'found')
            
            self.hovert.names.append('found')
            
            if label:
                labels = LabelSet(x = 'mz_obs', y = 'y', 
                          text = 'peak_label', 
                          x_offset = 4, y_offset = 9,
                          text_color = Category10[10][1],
                          source = source, angle = 1.57,
                          text_font_size = '11pt',
                          render_mode = 'canvas')
                
                self.p.add_layout(labels)
                
            if neutral_loss:
                for l in neutral_loss:
                    NP(l).plotNeutralLoss(
                            self.ms2search.found,
                            self.ms2search.msanalyzer.data.raw,
                            self.p,
                            line_color = 'grey',
                            legend_label = 'loss %s' % l,
                            line_dash = 'dotted'
                        )
            