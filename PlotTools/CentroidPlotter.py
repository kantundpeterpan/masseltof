#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 14:36:58 2020

@author: kantundpeterpan
"""

from matplotlib import pyplot as plt
from matplotlib import cm 

import numpy as np

class CentroidPlotter():
    
    @classmethod
    def spectrum_overlay(cls, ax, mzs,
                         ymin=0, ymax=100,
                         linestyle='--',
                         color='k',
                         alpha=0.8,
                         zorder=-1,
                         **kwargs):
        return ax.vlines(
            mzs,
            ymin,
            ymax,
            alpha=alpha,
            zorder=zorder,
            colors=color,
            linestyle=linestyle,
            **kwargs)
    
    @classmethod
    def SimpleBandPlot(cls, mzs, plot_type = 'horizontal',
                       minimum=0, maximum=1,
                       **kwargs):
        
        type_lookup = {
                'horizontal':'vlines',
                'vertical':'hlines'
            }
        
        if 'ax' in kwargs.keys():
            ax = kwargs.pop('ax')
            fig = ax.figure
        else:
            fig, ax = plt.subplots()
            
        getattr(ax,type_lookup[plot_type])(
            mzs,
            minimum,
            maximum,
            **kwargs)
        
        return fig, ax
        
    @classmethod
    def ContourPlot(cls, mzs, ints,
                    cmap = 'Greys_r',
                    ax_facecolor = 'k',
                    plot_type='horizontal',
                    **kwargs):
        
        X, Y = np.meshgrid(mzs, range(2))
        Z = np.vstack([ints, ints])
        
        if 'ax' in kwargs.keys():
            ax = kwargs.pop('ax')
            fig = ax.figure
        else:
            fig, ax = plt.subplots()
            
        orientation = {
                'horizontal':(X,Y),
                'vertical':(Y,X)
            }
        
        ax.set_facecolor(ax_facecolor)
        ax.contour(*orientation[plot_type],
                   Z, cmap=cmap, **kwargs)
        
        return fig, ax
    
    @classmethod
    def ComparePlot(cls,
                    mzs_exp, ints_exp,
                    mzs_theor,
                    cmap = 'Greys_r',
                    plot_type='vertical'):
        
        plot_type_map = {
                'horizontal':{'sharex':True},
                'vertical':{'sharey':True}
            }
        
        fig, axs = plt.subplots(ncols=1+len(mzs_theor),
                                **plot_type_map[plot_type])
        
        axs = axs.ravel()
        
        cls.ContourPlot(mzs_exp, ints_exp, ax=axs[0],
                        cmap=cmap,
                        plot_type=plot_type)
        
        for ax, mz in zip(axs[1:], mzs_theor):
            cls.SimpleBandPlot(mz, plot_type=plot_type, ax=ax)
            
        fig.tight_layout()
            
        return fig, axs
    