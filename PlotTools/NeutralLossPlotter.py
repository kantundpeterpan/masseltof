#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 13:51:17 2020

@author: kantundpeterpan
"""

import numpy as np
import pandas as pd#

from functools import partial
from itertools import cycle
from bokeh.palettes import Category10

import bokeh.plotting
from matplotlib.axes import Axes

class NeutralLossPlotter():
    
    loss_masses = {
            'H2O':18.010565,
            'NH3':17.026549,
        }
    
    def __init__(self, loss):
        if loss in self.loss_masses:
            self.loss = self.loss_masses[loss]
        else:
            try:
                float(loss)
            except:
                raise ValueError('Provide either formulas of known neutral loss fragments or'
                                 'numeric argument.')
            self.loss = loss
        
    def plotNeutralLoss(self, peaks, data, ax,
                        x = 'x', y = 'y', 
                        pid = 'spec_id',
                        factor = 1.2,
                        **plot_kws):
    
        
        coords = self._plotmatrix(peaks = peaks,
                                 data = data,
                                 x = x, 
                                 y = y)
        
        if isinstance(ax, Axes):
            collections = []
            for idx, row in coords.iterrows():
                
                y = max(row.py, row.ly)*factor
                
                collections.append(ax.vlines(
                        x = row.px,
                        ymin = row.py,
                        ymax = y,
                        **plot_kws
                    ))
                
                collections.append(ax.vlines(
                        x = row.lx,
                        ymin = row.ly,
                        ymax = y,
                        **plot_kws
                    ))
                
                collections.append(ax.hlines(
                        y = y,
                        xmin = row.lx,
                        xmax = row.px,
                        **plot_kws
                    ))
                
            return collections
        
        elif isinstance(ax, bokeh.plotting.Figure):
            
            offset = ax.y_range.end*0.1
            
            for idx, row in coords.iterrows():
                y = max(row.py, row.ly)*(1+(1/row.py)) + offset*0.75
                
                ax.step([row.px, row.px, row.lx, row.lx],
                        [row.py + offset, y + offset, y + offset, row.ly],
                        **plot_kws)
            
    
    def removeLines(self, coll):
        while len(coll):
            coll[0].remove()
        return True
        
    
    def _getLines(self, peakid, data,
                 x = 'x', y = 'y'):

        peak_x = getattr(data, x).loc[peakid]
        peak_y = getattr(data, y)[peakid]
        
        loss_x = peak_x - self.loss
        loss_y = getattr(data, y)[
            np.searchsorted(
                getattr(data, x), loss_x
            )
            ]
        
        return peak_x, peak_y, loss_x, loss_y
    
    def _plotmatrix(self, peaks, data, x = 'x', y = 'y', pid = 'spec_id'):

        _map_func = partial(self._getLines, data = data, x = x, y = y)
        
        #px, lx, py, ly
        coords = [*map(_map_func, getattr(peaks, pid).values.astype(int))]
        
        coords = pd.DataFrame(coords, columns = ['px', 'py', 'lx', 'ly'])
        
        return coords