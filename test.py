#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:50:06 2019

@author: kantundpeterpan
"""

import sys
from PyQt5 import QtGui
from PyQt5 import QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5 import FigureManagerQT
from matplotlib.backend_managers import ToolManager
from matplotlib.backend_tools import add_tools_to_manager
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
#plt.rcParams['toolbar'] = 'toolmanager'
import random

class Window(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = Figure()


        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)
        #self.manager = FigureManagerQT(self.canvas,1)
        self.figure.canvas.mpl_connect('key_press_event', self.home)
        #self.toolmanager = ToolManager(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        #self.canvas.manager.toolbar = self.toolbar

        # Just some button connected to `plot` method
        self.button = QtWidgets.QPushButton('Plot')
        self.button.clicked.connect(self.plot)

        # set the layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.button)
        self.setLayout(layout)
        
        #add a Tools
        #add_tools_to_manager(self.toolmanager)
    
    def home(self):
            self.toolbar.home()
    
    def plot(self):
        ''' plot some random stuff '''
        # random data
        data = [random.random() for i in range(10)]

        # create an axis
        ax = self.figure.add_subplot(111)

        # discards the old graph
        ax.clear()

        # plot data
        ax.plot(data, '*-')

        # refresh canvas
        self.canvas.draw()