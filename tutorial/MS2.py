#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 11:31:41 2021

@author: kantundpeterpan
"""
import sys
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python/')

from masseltof import MSanalyzer
from masseltof.Species import SpeciesX
from masseltof.LabelDict import LabelDict

tetra = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-C_ter*Ala'

#h1 labeling patterns
h1G_pattern = '+--------'
h1M_pattern = '--+------'

#tetra h1 species
##h1G
tetra_h1G = SpeciesX(
        tetra,
        MSanalyzer,
        labeling = LabelDict.from_string('C13N15'),
        labeling_pattern = h1G_pattern
    )

##h1M
tetra_h1M = SpeciesX(
        tetra,
        MSanalyzer,
        labeling = LabelDict.from_string('C13N15'),
        labeling_pattern = h1M_pattern        
    )

#calculate framentation patterns
tetra_h1G.fragment(
        min_length = 1, #minimum length of theoretical product ions
        multi = True, #use multiple jobs
        use_checkers = True, #checker functions can be used to apply constraints on
                             #the calculation of fragments
        checkers = ['checkGlcNAc'] #here we want only those product ions that contain 
                                   #intact GlcNAc moieties, substantially reduces the number 
                                   #of fragments
    )

tetra_h1G.ms2['id'] = 'h1G'

tetra_h1M.fragment(
        min_length = 1,
        multi = True,
        use_checkers = True,
        checkers = ['checkGlcNAc']
    )

tetra_h1M.ms2['id'] = 'h1M'

ms = MSanalyzer(filename = './data/m15_tetrah1_ms2/20190606_HEINER_XP3_T60_F4_MSMS_949.43_CE_46_60_BA5_01_4121.mzML',
                fast=True,
                fast_stream=True,
                plot_tic = False)

ms.mzml_parser.combine_spectra_iter(0.12,0.5, remove_zero=True, binning = False)

#constant baseline removal
noise = 50
ms.data.raw.y = ms.data.raw.y - 50
ind = ms.data.raw.y < 0
ms.data.raw.y.loc[ind] = 0

from masseltof.MassWorkflows import MS2Search

ms2 = MS2Search([tetra_h1G, tetra_h1M], #species that will be used for peak assignment
              int_range=(0.12,0.5), #range of retention time for spectra combination
              file=ms.path, #mzML filename
              limit_ppm=20, #maximum absolute deviation in ppm of peaks
              ms = ms, #MSanalyzer instance
              thres_abs=True, #supplied threshold value for peak detection in absolute values
              thres=noise, #value for minimal peak intensity
              savitzky_golay=False)

from masseltof.PlotTools.MS2Bokeh import MS2Bokeh

ms2_report = MS2Bokeh(
                ms2,
                neutral_loss=['H2O']
                )

from bokeh.plotting import show
from bokeh.io import save

show(ms2_report.layout)

save(ms2_report.layout,
     'filename.html')