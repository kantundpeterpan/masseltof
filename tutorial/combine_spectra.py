#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:56:37 2021

@author: kantundpeterpan
"""

import sys
sys.path.append('/home/kantundpeterpan/my_stuff_on_server/Python')

from MassAnalyzer import MSanalyzer as msa
x = msa(filename='./data/bwd6_tetra/3110_BWD6LDT_T0_F17_BA2_01_5956.mzML',
        plot_tic=False)

x.mzml_parser.combine_spectra(0.3, 0.4)
x.analysis.plot_spectrum(show = False, cursor = False)
'''
x.analysis.ax.set_xlim(941, 987)
x.analysis.fig.tight_layout()
x.analysis.ax.autoscale(axis = 'y')
x.analysis.fig.canvas.draw()
'''
x.analysis.fig.show()
