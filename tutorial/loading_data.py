#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 11:56:37 2021

@author: kantundpeterpan
"""

import sys
sys.path.append('/home/kantundpeterpan/my_stuff_on_server/Python')

from MassAnalyzer import MSanalyzer as msa

x = msa(filename='./data/bwd6_tetra/3110_BWD6LDT_T0_F17_BA2_01_5956.mzML',
        plot_tic=False)

x.analysis.plot_tic(show = False)
x.analysis.tic_fig.show()
