#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 16:25:26 2020

@author: kantundpeterpan
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 13:46:39 2020

@author: kantundpeterpan
"""
import pickle as pkl
import os

from ... import MSanalyzer
from ...LabelDict import LabelDict
from ...Species import SpeciesX

path = os.path.abspath(os.path.dirname(__file__))

class MS2DBMeta(type):
    def __getitem__(cls, name):
        return cls.db[name]


class MonomersMS2DB(metaclass =  MS2DBMeta):
    
    filename = 'monomersMS2.pkl'
    
    keys = [
        'Tri',
        'Tetra',
        'Penta',
        'UDP5'
            ]
    
    tri = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*C_ter*DAP'
    tetra = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-C_ter*Ala'
    penta = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-Ala-C_ter*Ala'
    udp5 = 'U-2PRibose-GlcN(-Ac)-Lac-Ala-Glu-N_ter*DAP-Ala-C_ter*Ala'
    
    species_str_map = {
            key:species_str for key, species_str in zip(keys, (tri, tetra,
                                                               penta, udp5))
        }
    
    if not os.path.exists(os.path.join(path, filename)):
        pkl.dump({key:{} for key in keys}, open(os.path.join(path, filename), 'wb'))
        
    db = pkl.load(open(os.path.join(path, filename), 'rb'))

    @classmethod
    def _key_check(cls):
        for key in cls.keys:
            if key not in cls.db.keys():
                cls.db[key] = {}
        
        cls._save()
    
    @classmethod
    def _save(cls):
        pkl.dump(cls.db, open(os.path.join(path, cls.filename), 'wb'))
        
    @classmethod
    def createHybrid(cls, parent, label, labeling, labpat, overwrite = False,
                     **frag_kws):
        try:
            assert parent in cls.db.keys()
        except:
            raise ValueError('unknown parent molecule')
            
        if label in cls.db[parent].keys() and not overwrite:
            raise AssertionError('label already in use and overwrite disabled')
            
        tmp_hybrid = SpeciesX(
                cls.species_str_map[parent],
                MSanalyzer,
                labeling = LabelDict.from_string(labeling),
                labeling_pattern = labpat
            )
        
        tmp_hybrid.fragment(
                min_length=frag_kws.pop('min_length', 1),
                multi = frag_kws.pop('multi', True),
                use_checkers = frag_kws.pop('use_checkers', True),
                checkers = frag_kws.pop('checkers', ['checkGlcNAc'])
            )
        
        cls.db[parent][label] = tmp_hybrid
        cls._save()
        
        return cls.db[parent][label]

    @classmethod
    def addHybrid(cls, parent, label, hybrid):
        
        try:
            assert parent in cls.db.keys()
        except:
            raise ValueError('unknown parent molecule')
