#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 13:02:42 2020

@author: kantundpeterpan
"""

import sys
sys.path.append('/home/kantundpeterpan/my_stuff_on_server/Python/')

import pickle as pkl

import MassAnalyzer
from MassAnalyzer.Species import SpeciesX
from MassAnalyzer import MSanalyzer

import os
wd = os.path.dirname(MassAnalyzer.MS2Helper.__file__)
root_wd = os.path.join(wd, 'FragDB', 'Tetra_Tetra')
os.chdir(root_wd)

species_str = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-Ala-DAP(-C_ter*Ala)-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'

hybrid_name = 'all_heavy'

"""
labpats  = ['+----+++----------',
            '--+--+++----------',
            '-----+++------+---',
            '-----+++--------+-',
            '+--------+-++-----',
            '--+------+-++-----',
            '---------+-++-+---',
            '---------+-++---+-']
"""

"""
labpats  = ['---------+++++++++',
            '+++++++++---------']
"""

"""
labpats  = ['++++++++++++++++--',
            '--++++++++++++++++']
"""

labpats  = ['++++++++++++++++++']

labeling = {'C':13, 'N':15}

hybrids = [SpeciesX(species_str, MSanalyzer(''),
                    labeling_pattern=labpat,
                    labeling=labeling)
           for labpat in labpats]

for h in hybrids:
    h.fragment(min_length=1)
    
pkl.dump(
    {labpat:h for labpat,h in zip(labpats, hybrids)},
    open('%s_ms2.pkl' % hybrid_name, 'wb')
    )
