#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 13:46:39 2020

@author: kantundpeterpan
"""
import pickle as pkl
import os

from ... import MSanalyzer
from ...LabelDict import LabelDict
from ...Species import SpeciesX

path = os.path.abspath(os.path.dirname(__file__))

class MS2DBMeta(type):
    def __getitem__(cls, name):
        return cls.db[name]


class DimersMS2DB(metaclass =  MS2DBMeta):
    
    filename = 'dimersMS2.pkl'
    
    keys = [
        'Tri-Tri',
        'Tetra-Tri',
        'Tri-Tetra',
        'Tetra-Tetra',
        'Tri-Penta'
            ]
    
    tritri = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-C_ter*DAP-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'
    tetratri = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-Ala-C_ter*DAP-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'
    tritetra = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-DAP(-C_ter*Ala)-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'
    tetratetra = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-Ala-DAP(-C_ter*Ala)-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'
    tripenta = 'GlcN(-Ac)-GlcN_Red(-Ac)-Lac-Ala-Glu-N_ter*DAP-DAP(-Ala-C_ter*Ala)-Glu-Ala-Lac-GlcN_Red(-Ac)-GlcN(-Ac)'
    
    species_str_map = {
            key:species_str for key, species_str in zip(keys, (tritri, tetratri,
                                                               tritetra, tetratetra,
                                                               tripenta))
        }
    
    if not os.path.exists(os.path.join(path, filename)):
        pkl.dump({key:{} for key in keys}, open(os.path.join(path, filename), 'wb'))
        
    db = pkl.load(open(os.path.join(path, filename), 'rb'))
    
    @classmethod
    def _save(cls):
        pkl.dump(cls.db, open(os.path.join(path, cls.filename), 'wb'))
        
    @classmethod
    def createHybrid(cls, parent, label, labeling, labpat, overwrite = False,
                     **frag_kws):
        try:
            assert parent in cls.db.keys()
        except:
            raise ValueError('unknown parent molecule')
            
        if label in cls.db[parent].keys() and not overwrite:
            raise AssertionError('label already in use and overwrite disabled')
            
        tmp_hybrid = SpeciesX(
                cls.species_str_map[parent],
                MSanalyzer,
                labeling = LabelDict.from_string(labeling),
                labeling_pattern = labpat
            )
        
        tmp_hybrid.fragment(
                min_length=frag_kws.pop('min_length', 1),
                multi = frag_kws.pop('multi', True),
                use_checkers = frag_kws.pop('use_checkers', True),
                checkers = frag_kws.pop('checkers', ['checkGlcNAc'])
            )
        
        cls.db[parent][label] = tmp_hybrid
        cls._save()
        
        return cls.db[parent][label]

    @classmethod
    def addHybrid(cls, parent, label, hybrid):
        
        try:
            assert parent in cls.db.keys()
        except:
            raise ValueError('unknown parent molecule')
