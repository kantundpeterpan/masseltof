#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 23:29:31 2020

@author: kantundpeterpan
"""

import numpy as np
import pandas as pd

from collections import Counter
from itertools import combinations
import networkx as nx

from multiprocessing import Pool
from functools import partial

class Fragmenter():
    
    @classmethod
    def fragment(cls, sequence, min_length=3,
                 use_checkers=True,
                 multi = False,
                 n_jobs=2,
                 checkers = ['checkGlcNAc', 'checkMurNAc']):
        
        if not multi:
            cg = []
            for i in range(min_length, len(sequence.mol.nodes)+1): 
                bla = combinations(list(sequence.mol.nodes),r=i) 
                for sg in bla:
                    nxsg = sequence.mol.subgraph(sg)
                    if nx.is_connected(nxsg):
                        if use_checkers:
                            total_check = sum([getattr(cls, chk)(sg) for chk in checkers])
                            if total_check == len(checkers):
                                m = cls.fragmentmass(nxsg, sequence)
                                cg.append((m, sg))
                        
                        if not use_checkers:
                            m = cls.fragmentmass(nxsg, sequence)
                            cg.append((m, sg))
                            
            cg = pd.DataFrame(cg, columns = ['M_H', 'frags'])
            print(cg.shape)
            
        if multi:
            _map_func = partial(cls.fragment_single_len,
                                sequence=sequence,
                                use_checkers=use_checkers,
                                checkers=checkers)
            
            with Pool(n_jobs) as p:
                cg = p.map(_map_func, range(min_length,
                                            len(sequence.mol.nodes)+1))

            cg = pd.concat(cg)
            print(cg.shape)
            
        cg = pd.pivot_table(cg, index = 'M_H', values = 'frags',
                            aggfunc = lambda x: [i for i in x]).reset_index()
        cg['n_frags'] = cg.frags.apply(len)
                    
        return cg
    
    @classmethod
    def fragment_single_len(cls, length, sequence, use_checkers=True,
                            checkers = ['checkGlcNAc', 'checkMurNAc']):
        cg = []
        bla = combinations(list(sequence.mol.nodes),r=length) 
        for sg in bla:
            nxsg = sequence.mol.subgraph(sg)
            if nx.is_connected(nxsg):
                if use_checkers:
                    total_check = sum([getattr(cls, chk)(sg) for chk in checkers])
                    if total_check == len(checkers):
                        m = cls.fragmentmass(nxsg, sequence)
                        cg.append((m, sg))
                
                if not use_checkers:
                    m = cls.fragmentmass(nxsg, sequence)
                    cg.append((m, sg))
                    
        return pd.DataFrame(cg, columns = ['M_H', 'frags'])
                    
    
    @classmethod
    def fragmentmass(cls, subgraph, sequence, sig_no=6):
        '''hardcoded monocharged fragments'''
        
        m_h = sequence.msanalyzer.m_h
        
        base = sum([r[-1]['Residue'].monoisotopic_mass for r in subgraph.nodes(data=True)])
        
        if cls.checkCter(subgraph, sequence) and cls.checkNter(subgraph, sequence):
            return round(base + m_h, sig_no)
        
        elif not cls.checkCter(subgraph, sequence) and not cls.checkNter(subgraph, sequence):
            return round(base + m_h, sig_no)
        
        elif not cls.checkCter(subgraph, sequence) and cls.checkNter(subgraph, sequence):
            return round(base, sig_no)
        
        elif cls.checkCter(subgraph, sequence) and not cls.checkNter(subgraph, sequence):
            return round(base + m_h * 2, sig_no)
    
    @classmethod
    def checkNter(cls, subgraph, sequence):
        checker = sum([r[-1]['Residue'].N_ter for r in subgraph.nodes(data=True)])>0
        return checker
    
    @classmethod
    def checkCter(cls, subgraph, sequence):
        checker = sum([r[-1]['Residue'].C_ter for r in subgraph.nodes(data=True)])>0
        return checker
    
    @classmethod 
    def checkGlcNAc(cls, subgraph):
        codes = [s.split('!')[-1] for s in subgraph]
        counter = Counter(codes)
        checker = counter['Ac'] == (counter['GlcN']+counter['GlcN_Red'])
        return checker
        
    @classmethod 
    def checkMurNAc(cls, subgraph):
        codes = [s.split('!')[-1] for s in subgraph]
        counter = Counter(codes)
        checker = (counter['Lac'] == counter['GlcN_Red'])
        return checker
    