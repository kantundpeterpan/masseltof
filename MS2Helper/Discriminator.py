#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 16:44:07 2020

@author: kantundpeterpan
"""

import numpy as np
import pandas as pd

from orderedset import OrderedSet

class Discriminator():
    
    @classmethod
    def discriminate(cls, mzs1, mzs2, both=True):
        
        unique_msz1 = mzs1.loc[~np.in1d(mzs1.M_H, mzs2.M_H)]
        
        if both:
            unique_mzs2 = mzs2.loc[~np.in1d(mzs2.M_H, mzs1.M_H)]
            return unique_msz1, unique_mzs2
        
        return unique_msz1
    
    @classmethod
    def all_vs_all(cls,mols):
        unique = []
        args = OrderedSet(mols)
        for x in args:
            #creating one and complement
            one = set([x])
            other = args-one
            
            one = list(one)[0].ms2
            other = list(other)
            
            other = pd.concat([mol.ms2 for mol in other])
            unique.append(cls.discriminate(
                    one, other, both = False
                ))
            
        return unique
    
    @classmethod
    def groupedPeaks(cls, mols):
        df = pd.pivot_table(pd.concat([h.ms2 for h in mols]),
                       index = 'M_H', values='id',
                       aggfunc=lambda x:','.join(x)).reset_index()
        
        return df
        
        