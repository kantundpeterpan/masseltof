#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 11:18:21 2021

@author: kantundpeterpan
"""
from docx import Document
from docx.shared import RGBColor
from docx.enum.text import WD_LINE_SPACING
import re
import sys
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.FragDB.MS2DB import DimersMS2DB

def frag_cell_docx(frag, parent, isotop, cell, color_map,
                   write_termini=False,
                   db = None):
    p = cell.paragraphs[0]
    bond_runs = []
    for idx, res in enumerate(frag[0]):
        print(res)
        mol_res = parent[isotop].mol.nodes[res]['Residue']
        labeling = mol_res.labeling.to_string()
        
        for x in res.split('_'):
            r = p.add_run(re.sub('\d+!','', x).replace('_', ''))
            mol_res = parent[isotop].mol.nodes[res]['Residue']
            labeling = mol_res.labeling.to_string()
            r.font.color.rgb = RGBColor.from_string(color_map[labeling])
            
        if '_' in res:
            r.font.superscript = True
        if write_termini:
            if mol_res.C_ter:
                r = p.add_run('Cter')
                r.font.superscript = True
                r.font.color.rgb = RGBColor.from_string(color_map[labeling])
            if mol_res.N_ter:
                r = p.add_run('Nter')
                r.font.superscript = True
                r.font.color.rgb = RGBColor.from_string(color_map[labeling])
            
        if mol_res.C_ter and 'Ala' in res and db == DimersMS2DB:
            r = p.add_run(')')
            r.font.color.rgb = RGBColor(0,0,0)
            bond_runs[-1].text = '(-'
            
        if idx == len(frag[0])-1:
            if 'Ac' not in res:
                break
            else:
                r = p.add_run(')')
                r.font.color.rgb = RGBColor(0,0,0)
                break
            
        if 'GlcN' in res:
            r = p.add_run('(-')
        elif 'Ac' in res:
            r = p.add_run(')-')
        else:
            r = p.add_run('-')
            bond_runs.append(r)
        r.font.color.rgb = RGBColor(0,0,0)
        
def docx_table_unique(df, parent, color_map, doc, db, mz_format = '%.3f', ppm_format = '%.1f',
                      int_format = '%i', write_termini=False):
    
    doc.styles['Normal'].paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
    doc.styles['Normal'].paragraph_format.space_after = 0
    df = df.sort_values('mz_obs', ascending=False)
    
    dcols = ['Fragment', 'm/z_obs', 'm/z_calc', 'ppm', 'Intensity (a.u.)', 'Isotopologue']
    dtable = doc.add_table(rows = 1, cols = 6)
    for c, label in zip(dtable.rows[0].cells, dcols):
        for part in label.split('_'):
            c.paragraphs[0].add_run(part)
        if '_' in label:
            c.paragraphs[0].runs[-2].font.italic = True
            c.paragraphs[0].runs[-1].font.italic = True
            c.paragraphs[0].runs[-1].font.subscript = True
    
    for idx, row in df.iterrows():
        r = dtable.add_row()
        cell = r.cells[0]
        frag_cell_docx(row.frags,
                       db[parent],
                       row.id,
                       cell,
                       color_map,
                       write_termini=write_termini,
                       db = db)
        for idx, (cell, val) in enumerate(zip(r.cells[1:], [row.mz_obs, row.mz_calc, row.ppm, row.y, row['id']]),1):
            #print(idx)
            if idx < 3:
                cell.text = mz_format % val
            if idx == 3:
                cell.text = ppm_format % val
            if idx ==4:
                cell.text = int_format % val
            if idx == 5:
                cell.text = '%s' % val

    '''                
    for r in dtable.rows[1:]:
        tmp_txt = r.cells[0].text.replace('-Ac', '(-Ac)')
        r.cells[0].text = tmp_txt
        print(r.cells[0].text) 
        '''
    return doc

def docx_table_ambig(df, parent, color_map, doc, db, mz_format = '%.3f', ppm_format = '%.1f',
                      int_format = '%i', write_termini=True):

    doc.styles['Normal'].paragraph_format.line_spacing_rule = WD_LINE_SPACING.SINGLE
    doc.styles['Normal'].paragraph_format.space_after = 0
    dcols = ['Fragment', 'm/z_obs', 'm/z_calc', 'ppm', 'Intensity (a.u.)']
    dtable = doc.add_table(rows = 1, cols = 5)
    
    for c, label in zip(dtable.rows[0].cells, dcols):
        for part in label.split('_'):
            c.paragraphs[0].add_run(part)
        if '_' in label:
            c.paragraphs[0].runs[-2].font.italic = True
            c.paragraphs[0].runs[-1].font.italic = True
            c.paragraphs[0].runs[-1].font.subscript = True
            
    for idx, row in df.iterrows():
        print(idx)
        group_isotops = row.group.split(',') #isotopologues containing a fragment with this mass
        print(group_isotops)
        drow = dtable.add_row() #[dtable.add_row() for i in range(len(group_isotops))] #add rows for each distinct fragment = len(row.frags[0])
        #print(len(rows))
        #first row with isotopologue and all values
        cell = drow.cells[0]
        print(row.frags[0])
        frag_cell_docx(row.frags[0],
                       db[parent],
                       group_isotops[0],
                       cell,
                       color_map,
                       write_termini=write_termini,
                       db = db)
        for idx, (cell, val) in enumerate(zip(drow.cells[1:], [row.mz_obs, row.mz_calc, row.ppm, row.y]),1):
            if idx < 3:
                cell.text = mz_format % val
            if idx == 3:
                cell.text = ppm_format % val
            if idx ==4:
                cell.text = int_format % val
                
        # if there's only one fragment, check whether they are labeled differently
        # print(len(row.frags))
        if len(row.frags) == 1:
            print('got here')
            labelings = []
            for isotop in group_isotops:
                mol = db[parent][isotop].mol
                labelings.append([mol.nodes[r]['Residue'].labeling.to_string() for r in row.frags[0][0]])
            checks = []
            for i in range(len(labelings)):
                for j in range(len(labelings)):
                    checks.append(labelings[i] == labelings[j])
            #if they are differently labeled - add rows for other isotopologues and fill first cell
            if sum(checks)<len(labelings)**2:
                for idx, isotop in enumerate(group_isotops[1:],1):
                    drow = dtable.add_row()
                    cell = drow.cells[0]
                    frag_cell_docx(row.frags[0],
                           db[parent],
                           isotop,
                           cell,
                           color_map,
                           write_termini=write_termini, 
                           db = db)
                    
        # if there are more than two fragments lookup them up in the corresponding mol
        # and add a row, fill the first cell
        for i in range(len(row.frags[1:])):
            drow = dtable.add_row()
            cell = drow.cells[0]
            frag_cell_docx(row.frags[i+1],
               db[parent],
               group_isotops[i+1],
               cell,
               color_map,
               write_termini=write_termini,
               db = db)            
                    
    return doc