#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 14:58:54 2021

@author: kantundpeterpan
"""

import pylatex as pl
from pylatex import Document, Tabular, LongTable
from pylatex import TextColor as TC
import sys
import re

import sys
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python')
from MassAnalyzer.MS2Helper.FragDB.MonomersMS2DB import MonomersMS2DB
from MassAnalyzer.MS2Helper.FragDB.MS2DB import DimersMS2DB
from MassAnalyzer import IsoDB

acceptor_map = {
        'Tri':99,
        'Tetra':99,
        'Tri-Tri':8,
        'Tetra-Tri':9,
        'Tri-Tetra':8,
        'Tetra-Tetra':9,
        'Tri-Penta':8
    }

def frag_cell_tex(frag, parent, isotop, color_map, db, write_termini):
    
    parent_str = parent
    acc_start = acceptor_map[parent_str]
    parent = db[parent]
    
    slist = []
    bond_runs = [] #keep indices
    #xspecies = DimersMS2[parent][isotop]
    for idx, res in enumerate(frag[0]):
        #print(res)
        mol_res = parent[isotop].mol.nodes[res]['Residue']
        labeling = mol_res.labeling.to_string()
        res_no = int(re.findall('\d+', res)[0])
        color = color_map[labeling]
        
        for x in res.split('_'):
            r_string = re.sub('\d+!', '', x)
            #print(r_string)
            
            if 'Red' not in r_string and not 'Ac' in r_string:
                if not 'Ala' in res:
                    if res_no >= acceptor_map[parent_str]:
                        #print('acc - ', r_string)
                        slist.append('\\underline{')
                        slist.append(TC(color, r_string).dumps())
                        slist.append('}')
                    else:
                        slist.append(TC(color, r_string).dumps())
                        
                elif 'Ala' in res:
                    if not mol_res.C_ter:
                        
                        if res_no >= acceptor_map[parent_str]:
                            slist.append('\\underline{')
                            slist.append(TC(color, r_string).dumps())
                            slist.append('}')
                        
                        else:
                            slist.append(TC(color, r_string).dumps())
                            
                    elif not write_termini and res_no >= acceptor_map[parent_str]:
                        slist[-1] = '\\underline{'
                        slist.append('(-')
                        slist.append(TC(color, r_string).dumps())
                        slist.append(')')
                        slist.append('}')
                    
                    else:
                        slist[-1] = '(-'
                        slist.append(TC(color, r_string).dumps())
                        slist.append(')')
                        
                if write_termini:
                    if mol_res.N_ter:
                        slist.append('\\textsuperscript{'+TC(color, 'Nter').dumps() + '}')
                        #print('N_ter')
                    if mol_res.C_ter and not 'Ala' in res:
                        slist.append('\\textsuperscript{'+TC(color, 'Cter').dumps() + '}')
                        #print('C_ter')
                    elif mol_res.C_ter and 'Ala' in res:
                        if db == DimersMS2DB:
                            slist[-1] = '(-'
                        slist.append(TC(color, r_string).dumps())
                        slist.append('\\textsuperscript{'+TC(color, 'Cter').dumps() + '}')
                        if db == DimersMS2DB:
                            slist.append(')')
                        
                if not idx == len(frag[0])-1:   
                    if res_no >= acceptor_map[parent_str]:
                        slist.append('\\underline{')                     
                        slist.append('-')
                        slist.append('}')
                    else:
                        slist.append('-')
            
            elif 'Red' in r_string:
                if res_no >= acceptor_map[parent_str]:
                        slist[-1]='\\underline{'
                        slist.append('\\textsuperscript{'+TC(color, 'Red').dumps() + '}}')
                        #slist.append('}')
                else:
                    slist[-1] = '\\textsuperscript{'+TC(color, 'Red').dumps() + '}'
                    
                slist.append('-')
                
                #if res_no >= acceptor_map[parent_str]:
                 #       slist.append('}')
                
            elif 'Ac' in r_string:
                if res_no >= acceptor_map[parent_str]:
                        slist[-1]='\\underline{(-'
                else:
                    slist[-1] = '(-'
                    
                slist.append(TC(color, r_string).dumps())
                
                if not idx == len(frag[0])-1:
                    slist.append(')-')
                else:
                    slist.append(')')
                
                if res_no >= acceptor_map[parent_str]:
                    slist.append('}')
        
        #slist.append(r_string.replace('_', ''))
        
    return pl.NoEscape(''.join(slist))

def latex_table_parent(parent_str, parent_isotops, prec_mz,
                       color_map, db, mz_format = '%.3f', ppm_format = '%.1f',
                      int_format = '%i', write_termini=False, alignment = 'lrrr'):
    
    dcolumns = ['Parent (MS\\textsuperscript{1})',
                '$m/z_{obs}$',
                '$m/z_{calc}$',
                'ppm']

    dtable = LongTable(alignment, booktabs=True)
    dtable.add_row(dcolumns, escape=False)
    dtable.add_hline()

    for parent_isotop in parent_isotops:
        cells = []
        
        cells.append(
               frag_cell_tex(
                   [tuple(db[parent_str][parent_isotop].node_list),],
                   parent_str,
                   parent_isotop,
                   color_map,
                   db,
                   write_termini=write_termini)
               ) 
            
        cells.append(
                mz_format % (prec_mz)
            )
    
        mz_calc = db[parent_str][parent_isotop].mass + IsoDB.H[1]
        
        cells.append(
                mz_format % (mz_calc)
            )
        
        ppm = (prec_mz - mz_calc)/mz_calc *1000000
        
        cells.append(ppm_format % ppm)
            
        dtable.add_row(cells)
    
    return dtable
    
def latex_table_unique(df, parent_str, color_map, db, mz_format = '%.3f', ppm_format = '%.1f',
                      int_format = '%i', write_termini=False, alignment = 'lrrrrr'):
    
    df = df.sort_values('mz_obs', ascending=False)
    dcolumns = ['Fragment',
           '$m/z_{obs}$', '$m/z_{calc}$',
           '$ppm$',
           '$Intensity (a.u.)$',
           'Isotopologue']

    dtable = LongTable(alignment, booktabs=True)
    dtable.add_row(dcolumns, escape=False)
    dtable.add_hline()
    
    #parent = db[parent_str]
    
    #fill rows
    for idx, row in df.iterrows():
        cells = []
        cells.append(frag_cell_tex(row.frags, parent_str, row.id, color_map, db, write_termini=write_termini))
        
        for idx, val in enumerate([row.mz_obs, row.mz_calc, row.ppm, row.y, row['id']]):
            #print(idx)
            if idx < 2:
                cells.append(mz_format % val)
            if idx == 2:
                cells.append(ppm_format % val)
            if idx == 3:
                cells.append(int_format % val)
            if idx == 4:
                cells.append('%s' % val)
        #print(cells)
        dtable.add_row(cells)

    return dtable

def latex_table_ambig(df, parent_str, color_map, db, mz_format = '%.3f', ppm_format = '%.1f',
                      int_format = '%i', write_termini=True, alignment = 'lrrrr'):

    df = df.sort_values('mz_obs', ascending=False)
    dcolumns = ['Fragment',
           '$m/z_{obs}$', '$m/z_{calc}$',
           '$ppm$',
           '$Intensity (a.u.)$']

    dtable = LongTable(alignment, booktabs=True)
    dtable.add_row(dcolumns, escape=False)
    dtable.add_hline()
    
    parent = db[parent_str]
            
    for idx, row in df.iterrows():
        #print(idx)
        group_isotops = row.group.split(',') #isotopologues containing a fragment with this mass
        print(idx, group_isotops)
        #print(row.frags[0])
        cells = []#[dtable.add_row() for i in range(len(group_isotops))] #add rows for each distinct fragment = len(row.frags[0])
        cells.append(
            frag_cell_tex(row.frags[0],
                          parent_str,
                          group_isotops[0],
                          color_map,
                          db,
                          write_termini=write_termini)
            )
        for idx, val in enumerate([row.mz_obs, row.mz_calc, row.ppm, row.y]):
            #print(idx)
            if idx < 2:
                cells.append(mz_format % val)
            if idx == 2:
                cells.append(ppm_format % val)
            if idx == 3:
                cells.append(int_format % val)
            if idx == 4:
                cells.append('%s' % val)
        #print(cells)
        dtable.add_row(cells)
        
        
        # if there's only one fragment, check whether they are labeled differently
        # print(len(row.frags))
        print('no frags', len(row.frags))
        print(row.frags)
        if len(row.frags) == 1:
            print('got here')
            labelings = []
            for isotop in group_isotops:
                #print(parent, isotop)
                mol = parent[isotop].mol
                labelings.append([mol.nodes[r]['Residue'].labeling.to_string() for r in row.frags[0][0]])
            checks = []
            for i in range(len(labelings)):
                for j in range(len(labelings)):
                    checks.append(labelings[i] == labelings[j])
            #if they are differently labeled - add rows for other isotopologues and fill first cell
            if sum(checks)<len(labelings)**2:
                for idx, isotop in enumerate(group_isotops[1:],1):
                    cells = []
                    cells.append(
                        frag_cell_tex(row.frags[0],
                                      parent_str,
                                      isotop,
                                      color_map,
                                      db,
                                      write_termini=write_termini)
                        )
                    dtable.add_row(cells, strict=False)
            continue
        # if there are more than two fragments lookup them up in the corresponding mol
        # and add a row, fill the first cell
        for i in range(len(row.frags[1:])):
            cells = []
            cells.append(
                frag_cell_tex(row.frags[i+1],
                              parent_str,
                              group_isotops[i+1],
                              color_map,
                              db,
                              write_termini=write_termini)
                )
            dtable.add_row(cells, strict=False)
            
    return dtable

'''

#get     
first = pd.concat(y.found_unique)[['frags', 'id']].apply(lambda x:frag_string_tex(x.frags[0], parent, x.id), axis=1)

second = first.apply(lambda x:'-'.join(x))
second = second.apply(lambda x:x.replace('GlcNRed', 'GlcN\\textsuperscript{Red}'))
#second = second.apply(lambda x:re.sub('\{.*Ac\}\}', '{\\textcolor{light}{GlcN\\textsuperscript{Red}}}(-{\\textcolor{light}{Ac}})', x))
second = second.apply(lambda x:re.sub('-\{.*Ac\}\}', '(-{\\textcolor{light}{Ac}})', x))
df_cols = ['mz_obs', 'mz_calc', 'ppm', 'y']

table = pd.concat(y.found_unique).set_index(second)[df_cols].sort_values('mz_obs', ascending=False)
table.index.name = 'Fragment'
table.y = table.y.astype(int)
table.columns = ['$m/z_{obs}$', '$m/z_{calc}$', '$ppm$', '$Intensity (a.u.)$']

rawlatex = table.to_latex(escape=False, longtable=False).replace(r'\toprule','\\hline').replace('\\midrule', '\\hline').replace('\\bottomrule', '\\hline')
with open('%s.tex' % basename, 'w') as f:
    f.write(rawlatex)

latex = open('../template.tex').read()

with open('ms2_table_%s.tex' % basename, 'w') as f:
    f.write(latex.replace('${text_here}$', table.to_latex(escape=False, longtable=False).replace(r'\toprule','\\hline').replace('\\midrule', '\\hline').replace('\\bottomrule', '\\hline')))

call('pdflatex ms2_table_%s.tex' % basename, shell=True)
'''