#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 14:56:39 2021

@author: kantundpeterpan
"""
from .DOCX import *
from .LaTeX import *