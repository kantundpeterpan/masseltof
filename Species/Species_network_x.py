#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 11:35:18 2019

@author: kantundpeterpan
"""
import networkx as nx

from .Residue_class import Residue
from ..MS2Helper import Fragmenter

class SpeciesX(object):
    
    def __init__(self, species_str, msanalyzer, **kwargs):

        self.msanalyzer = msanalyzer
        self.species_str = species_str
        
        #!!!
        self.species = self
        
        self.mol = nx.Graph()
        
        for i,res in enumerate(species_str.split('-')):
            code = res.strip(')').strip('(')
            
            if '*' in code:
                modifiers = code.split('*')[:-1]
                code = code.split('*')[-1]
                tmp_res = Residue(residue=code,
                                  sequence=self,
                                  **{modifier:True for modifier in modifiers})
            else:
                tmp_res = Residue(residue=code,
                                  sequence=self)
                
            g_res_name = str(i) + '!' + code
            
            self.mol.add_node(g_res_name)
            self.mol.nodes[g_res_name]['Residue'] = tmp_res
            
        self.node_list = list(self.mol.nodes)
        
        nodes_split = species_str.split('-')
        #print(len(self.node_list))
        
        i = 0
        while i < len(self.node_list):
            #print(i)
            if i == len(self.node_list)-1:
                break
            if ')' not in nodes_split[i]:
                self.mol.add_edge(self.node_list[i], self.node_list[i+1])
                
            if ')' in nodes_split[i]:
                #connect root to neighbor when branch is closed
                self.mol.add_edge(root, self.node_list[i+1])
                
            if '(' in nodes_split[i] and i != (len(self.node_list)-2):
                #save root node for branch
                root = self.node_list[i]    
                
                #print(nodes_split[i])
                
                #hard coded one residue branch
                #self.mol.add_edge(self.node_list[i], self.node_list[i+2])
            i+=1
            
        if 'labeling_pattern' in kwargs.keys():
            labpat = kwargs.pop('labeling_pattern')
            labeling = kwargs.pop('labeling')
            self.apply_labeling_pattern(labpat, labeling)
            
        self.calc_mass()
       # print(self.mass)
        
    def calc_mass(self):
        self.mass = sum([r[-1]['Residue'].monoisotopic_mass for r in self.mol.nodes(data=True) if 'Residue' in r[-1].keys()])

    def apply_labeling_pattern(self, pattern, labeling):
        try:
            assert len(pattern) == len(self.node_list)
        except:
            print('Pattern cannot be applied to residues')
            return
        
        for i in range(len(pattern)):
            if pattern[i] == '+':
                self.mol.nodes[self.node_list[i]]['Residue'].labeling = labeling
        return True
    
    def fragment(self, multi = True, **kwargs):
        self.ms2 = Fragmenter.fragment(self, multi = multi, **kwargs)
        return True
