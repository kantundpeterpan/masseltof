#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 12:22:54 2019

@author: kantundpeterpan
"""
from .Residue_class import Residue

class Sequence():
    '''Object to store information of a molecular Species object'''
    
    def __init__(self, residue_string = '', species = None):
        
        self.species = species
        self.sequence = residue_string
        
        self.residues = []
        
        for residue in residue_string.split('+'):
            
            if '*' in residue:
                modifiers = residue.split('*')[:-1]
                residue = residue.split('*')[1]
                
                self.residues.append(Residue(residue=residue, sequence = self,
                                             **{modifier:True for modifier in modifiers}))
            
            else:
                self.residues.append(Residue(residue=residue, sequence = self))
        
        
        self.length = len(self.residues)
        
        self.calc_mass()        
        self.calc_formula()
    
    def apply_labeling_pattern(self, pattern, labeling):
        try:
            assert len(pattern) == len(self.residues)
        except:
            print('Pattern cannot be applied to residues')
            return
        
        for i in range(len(pattern)):
            if pattern[i] == '+':
                self.residues[i].labeling = labeling
        return True
    
    def get_possible_msms_indices(self):
        msms = np.array(list(product([True, False], repeat=self.length)))
        possible_msms = [] 
        for i in range(1,self.length): 
            temp_msms = msms[[np.sum(x) == i for x in msms]] 
            for s in temp_msms: 
                for j in range(len(s)+1):  
                    if str(list(s[j:j+i])) == str([True]*i):  
                        possible_msms.append(s) 
        self.possible_msms = possible_msms
    
    def calc_mass(self):
        self.mass = sum([residue.monoisotopic_mass for residue in self.residues])
        
    def calc_formula(self):
        self.formula_dict = {}
        for residue in self.residues:
            for key, value in residue.formula_dict.items():
                if key not in self.formula_dict.keys():
                    self.formula_dict[key] = value
                else:
                    self.formula_dict[key]+=value
        self.formula = ''.join([str(key)+str(int(value)) for (key,value) in self.formula_dict.items() if value != 0])
