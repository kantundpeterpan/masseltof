#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 12:24:10 2019

@author: kantundpeterpan
"""

import sys

import pandas as pd
from ..MSanalyzer_class import MSanalyzer
from ..LabelDict import LabelDict

class Residue(object):
    '''Object for residues of a sequence'''
        
    N_ter = False
    C_ter = False
    
    def __repr__(self):
        return self.name+'*'+self.labeling.to_string()
    
    def __setattr__(self, name, value):
        
        #set the attribute via the default object function
        object.__setattr__(self, name, value)
        
        #recalculate the mass if the labeling was changed
        if name == 'labeling':
            self.monoisotopic_mass = self.calc_mass()
            if not self.sequence == None:
                self.sequence.calc_mass()
                
        if name == 'N_ter':
            #print('N_ter changed')
            if self.N_ter:
                self.formula_dict['H']+=1
            if not self.N_ter:
                self.formula_dict['H']-=1
            
            self.monoisotopic_mass = self.calc_mass()
            if not self.sequence == None:
                self.sequence.calc_mass()
            
        if name == 'C_ter':
            #print('C_ter changed')
            if self.C_ter:
                self.formula_dict['H']+=1
                self.formula_dict['O']+=1
                
            if not self.C_ter:
                self.formula_dict['H']-=1
                self.formula_dict['O']-=1
        
            self.monoisotopic_mass = self.calc_mass()
            if not self.sequence == None:
                self.sequence.calc_mass()
            
    
    def __add__(self):
        '''Adding two Residues should return a sequence
           Adding Residue to sequence should add this residue to the sequence Cter/Nter?'''
        pass
    
    
    def __init__(self, sequence = None, residue = '',
                 labeling=LabelDict({'C':12,'N':14}),
                 N_ter = False, C_ter = False):
        
        self.sequence = sequence
        self.name = residue
                
        residue_lookup = MSanalyzer.residues
        #residue_lookup.set_index('Residue', inplace = True)
        
        ##FIND SOLUTION FOR PRECALCULATED LABELINGS
        self.formula_dict = residue_lookup.loc[residue].iloc[:-2].to_dict()
        self.formula = ''.join([str(key)+str(value) for (key,value) in self.formula_dict.items() if value != 0])
        
        self.labeling = labeling

        if N_ter:        
            self.N_ter = N_ter
        if C_ter:
            self.C_ter = C_ter
    
        self.monoisotopic_mass = self.calc_mass()        
       
    def calc_mass(self):
        
        #dat is nix
        isotope_masses_lookup = MSanalyzer.isotope_masses
        #isotope_masses_lookup.set_index('element_symbol', inplace = True)
        #isotope_masses_lookup.dropna(inplace = True)
        
        ## USING PRECALCULATED RESIDUE MASSES AND NOT SUM FORMULA
        #if str(self.labeling) in MSanalyzer.residues.columns:
        if not bool(MSanalyzer.residues.isna().loc[self.name,str(self.labeling)]):
            add = 0
            
            ##WHAT IF H OR O is labeled?! Define N_ter/C_ter as modifiers?
            if self.N_ter:
                search = isotope_masses_lookup.loc[['H']] 
                ind = search['isotopic_composition'].values.argmax()
                add += search.iloc[ind, 1] 
            
            if self.C_ter:
                add += 17.0027396518
                
            return MSanalyzer.residues.loc[self.name,str(self.labeling)]+add
        
        mass = float(0)
        
        for element in self.formula_dict.keys():

            #find number of atoms of the element
            atoms = self.formula_dict[element]
            
            #lookup monoisotopic atomic mass for the element if element is labeled
            if element in self.labeling.keys():
                isotope = self.labeling[element]
                isotopic_mass = isotope_masses_lookup.loc[element][
                    isotope_masses_lookup.loc[element].isotope == isotope
                             ].atomic_mass.values[0]
            
            #lookup monoisotopic atomic mass for the element if element is not labeled                
            elif element not in self.labeling.keys():
                search = isotope_masses_lookup.loc[[element]] 
                ind = search['isotopic_composition'].values.argmax()
                isotopic_mass = search.iloc[ind, 1]

            mass+=atoms*isotopic_mass
        
        
        return mass
        
        