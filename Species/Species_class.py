#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 12:20:17 2019

@author: kantundpeterpan
"""
import pandas as pd
import numpy as np
from .Sequence_class import Sequence
from ..LabelDict import LabelDict
from collections import Counter
from tqdm import tqdm
from p_tqdm import p_map

#test for joblib
#from joblib.externals.loky import set_loky_pickler
#from joblib import parallel_backend
#from joblib import Parallel, delayed
#from joblib import wrap_non_picklable_objects

from pathos.multiprocessing import ProcessingPool as Pool

from itertools import product

#trying to bypass pickiling problems, (ref: http://qingkaikong.blogspot.com/2016/12/python-parallel-method-in-class.html)
def unwrap_self(arg, **kwargs):
    return Species.add_labeling_variant(*arg, **kwargs)


class Species(object):
    '''Provides an object to store information about a molecular species
       analyzed with MSanalyzer'''

    #__getstate__ and __setstate__ to enable pickling (ref: https://stackoverflow.com/questions/50888391/pickle-of-object-with-getattr-method-in-python-returns-typeerror-object-no)    
    def __getstate__(self):
        return vars(self)
    
    def __setstate__(self, state):
        vars(self).update(state)
    
    def __getattr__(self, name):
        if name.startswith('__') and name.endswith('__'):
            raise AttributeError
        if name in self.labeling_variants.index:
            return self.labeling_variants.loc[name]
    
    def __getitem__(self, key):
        if key in self.labeling_variants.index:
            return self.labeling_variants.loc[key]
        else:
            print('Invalid index')
            return
       
    def __init__(self, name = '', residue_string = '', msanalyzer = None, labeling_variants = None):
        self.msanalyzer = msanalyzer
        self.name = name
        self.sequence_str = residue_string
        #self.sequence = Sequence(residue_string, self)
        
        #string parsing
        self.plain_sequence = residue_string.split('+')
        self.plain_sequence = [s.split('*')[-1] for s in self.plain_sequence]
        self.plain_sequence = '+'.join(self.plain_sequence)
        
        self.length = len(self.sequence_str.split('+'))
        
        columns = ['description', 'monoisotopic_mass','M_H', 'M_2H', 'labeling', 'labeling_pattern']
           
        self.labeling_variants = pd.DataFrame(columns = columns)
        self.labeling_variants.set_index('description', inplace = True)
        
        if not labeling_variants == None:
            for variant in labeling_variants:
                description = variant[0]
                pattern = variant[1]
                labeling = LabelDict(variant[2])#{atom:isotope for (atom, isotope) in variant[2]}
                
                self.add_labeling_variant(description, pattern, labeling)
    
        self.msms = dict()
        
    def add_labeling_variant(self, description, labeling_pattern, labeling, multi=False):
        '''construct a Sequence object with the stored residue_string, apply
           the labeling using the labeling pattern, retrieve and store 
           desired information'''
           
        temp_seq = Sequence(residue_string = self.sequence_str,
                            species = self)
        
        if temp_seq.apply_labeling_pattern(labeling_pattern, labeling):
           
            df = pd.DataFrame({
                    #'description':description,
                    'monoisotopic_mass':temp_seq.mass,
                    'M_H':temp_seq.mass+self.msanalyzer.m_h,
                    'M_2H':(temp_seq.mass+2*self.msanalyzer.m_h)/2,
                    'labeling':[list(labeling.items())],#trying list() for pickling
                    'labeling_pattern':labeling_pattern}, index = [description])
        
            if not multi:
                self.labeling_variants = self.labeling_variants.append(df, ignore_index = False)
            
            if multi:
                return df
        else:
            return
    
    def add_labeling_variant_multi(self, values):
        description, labeling_pattern, labeling = values
        temp = self.add_labeling_variant(description=description,
                                         labeling_pattern=labeling_pattern,
                                         labeling=labeling, multi=True)
        #print(description, labeling_pattern, labeling)
        return temp
    
    def compute_all_variants(self, isotopes = {'C':13,'N':15}, n_jobs=3):
        '''exhaustive computation of all possible residue labeling combinations'''
        
        labpats = [''.join(bla) for bla in product(('+','-'), repeat=self.length)]
        n = list(range(len(labpats)))
        labeling = [isotopes]*len(n)
        
        #print('calculating...')

        res = tqdm(Pool(n_jobs).map(self.add_labeling_variant_multi, 
                                    list(zip(n,labpats,labeling))),
                   total = len(n)
                   )
     
        
        df = pd.concat(res)
        self.labeling_variants = self.labeling_variants.append(df, ignore_index = False)
        
    def translate_labeling_pattern(self, pattern):
        
        try:
            assert len(pattern) == len(self.sequence_str.split('+'))
        except:
            print('Sequence and labeling pattern mismatch')
        
        sequence_list = self.plain_sequence.split('+')
        
        
        cnt = Counter()
        for i in range(len(pattern)):
            if pattern[i] == '+':
                cnt[sequence_list[i]]+=1
        
        label_sums = list(cnt.items())
        label_sums = sorted(label_sums)
        
        return label_sums
    
    def get_msms_for_labeling(self, labeling_pattern, labeling):
        
        temp_seq = Sequence(residue_string=self.sequence_str, species = self)
        temp_seq.apply_labeling_pattern(labeling_pattern, labeling)
        fragments = temp_seq.residues
        
        for frag in fragments:
            if frag.N_ter:
                frag.N_ter = False
        
        poss_msms = self.get_possible_msms_indices()
        
        calc_msms = []
        
        for i in range(len(poss_msms)):
       
            name = '.'.join([s.name+str(s.labeling) for s in np.array(fragments)[poss_msms[i]]]) 
            mass_mplush = sum([s.monoisotopic_mass for s in np.array(fragments)[poss_msms[i]]])+self.msanalyzer.m_h 
            calc_msms.append([name, mass_mplush])
        
        msms_df = pd.DataFrame(calc_msms)
        msms_df[0] = msms_df[0].apply(lambda x:x.replace('\'', '')) 
        msms_df[0] = msms_df[0].apply(lambda x:x.replace('{', '('))
        msms_df[0] = msms_df[0].apply(lambda x:x.replace('}', ')')) 
        msms_df[0] = msms_df[0].apply(lambda x:x.replace(': ', ''))
        msms_df[0] = msms_df[0].apply(lambda x:x.replace(', ', ''))

        self.msms[labeling_pattern] = {str(labeling):msms_df}
    
    def get_possible_msms_indices(self):
        msms = np.array(list(product([True, False], repeat=self.length)))
        possible_msms = [] 
        for i in range(1,self.length): 
            temp_msms = msms[[np.sum(x) == i for x in msms]] 
            for s in temp_msms: 
                for j in range(len(s)+1):  
                    if str(list(s[j:j+i])) == str([True]*i):  
                        possible_msms.append(s) 
        return possible_msms
    
    def load_labeling_variants(self, csv_file):
        
        try:
            self.labeling_variants = pd.read_csv(csv_file, index_col=0)
            print('File loaded successfully')
        except:
            print('File could not be loaded')
            return
