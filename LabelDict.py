#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 14:28:04 2020

@author: kantundpeterpan
"""

from collections import OrderedDict
import re


class LabelDict(OrderedDict):
    
    def __init__(self, *args, **kwargs):
        OrderedDict.__init__(self, *args, **kwargs)
        
    def __repr__(self):
        return self.to_string()
        
    def to_string(self):
        return ''.join([''.join([x,str(y)]) for x,y in self.items()])
    
    @classmethod
    def from_string(cls, s):
        
        r_ele = re.compile('[A-Za-z]+')
        r_mass = re.compile('\d+')
        
        elements = r_ele.findall(s)
        mass = [int(m) for m in r_mass.findall(s)]
        
        tmp = sorted(zip(elements, mass), key = lambda x:x[1])
        
        return cls(tmp)
        
        