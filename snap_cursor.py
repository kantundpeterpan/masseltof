#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:33:53 2019

@author: kantundpeterpan
"""

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt

import matplotlib.pyplot as plt

import numpy as np
import peakutils

class SnaptoCursor(object):
    
    def __init__(self, analysis):
        
        self.analysis = analysis
        
        self.x = analysis.msanalyzer.data.raw.x
        self.y = analysis.msanalyzer.data.raw.y
        
        self.m_z = self.x.min()
        self.z = 1
        
        self.ax = analysis.ax
        
        self.ly = self.ax.axvline(color='k', alpha=0.2)  # the vert line
        self.ly.set_xdata(self.x.min())
        
        self.marker, = self.ax.plot(self.x.min(),self.y[self.x == self.x.min()], marker="o", color="crimson", zorder=3) 
        self.marker_minus = self.ax.plot(self.x.min(),self.y[self.x == self.x.min()], marker="o", color="crimson", zorder=3)[0] 
        self.marker_plus = self.ax.plot(self.x.min(),self.y[self.x == self.x.min()], marker="o", color="crimson", zorder=3)[0]
        self.txt = self.ax.text(self.x.min() + 0.7, self.y[self.x == self.x.min()] + 0.9, '')

        
        self.qcursor = QtGui.QCursor()
        
        self.pop_menu = QtWidgets.QMenu()
        
        action = self.pop_menu.addAction('1')
        action.triggered.connect(lambda: self.set_z(1))
        action = self.pop_menu.addAction('2')
        action.triggered.connect(lambda: self.set_z(2))
        action = self.pop_menu.addAction('3')
        action.triggered.connect(lambda: self.set_z(3))
        action = self.pop_menu.addAction('Annotate peak')
        action.triggered.connect(lambda: self.analysis.peak_label_dialog.initUI(self.m_z, self.z))
        action = self.pop_menu.addAction('Annotate peak - centroid')
        action.triggered.connect(lambda: self.analysis.peak_label_dialog.initUI(self.centroid_on_click(), self.z))
        
    def open_pop_menu(self, event):
        if event.button == 2:
            self.pop_menu.popup(self.qcursor.pos())
        
    def set_z(self, z):
        self.z = z
        self.ax.figure.canvas.draw_idle()
        
    def select_peak_range(self):
        
        data_from_input = plt.ginput(n = 2, show_clicks = True)
        peak_range = [x for (x,y) in data_from_input]
        
        return peak_range
    
    def centroid_on_click(self):
        peak_range = self.select_peak_range()
        
        xmin = peak_range[0]
        xmax = peak_range[1]
        
        centroid = self.analysis.calc_peak_centroid(xmin, xmax)
        
        print(centroid)
        self.analysis.draw_centroid(centroid)
        return centroid
        
        
    
    def mouse_move(self, event):
        if not event.inaxes:
            return
        
        m_z, y = event.xdata, event.ydata
        
        indx = np.searchsorted(self.x, [m_z])[0]
        
        self.m_z = self.x[indx]
        y = self.y[indx]

#         ind = (self.x > m_z - 0.05) & (self.x < m_z + 0.05)
        
#         y = self.y[ind].max()
#         x = self.x[self.y == y].values[0]
    
        self.ly.set_xdata(m_z)
        
        self.marker.set_data([m_z],[y])
        
        ind = np.searchsorted(self.x, [m_z-(1/self.z)])[0]
        y = self.y[ind]
        self.marker_minus.set_data([m_z-(1/self.z)],[y])
        
        ind = np.searchsorted(self.x, [m_z+(1/self.z)])[0]
        y = self.y[ind]
        self.marker_plus.set_data([m_z+(1/self.z)],[y])
        
        m_z = round(event.xdata,4)
        y = event.ydata
            
        #deconvolute with given z
        z = int(self.z)
        mass = round((m_z*z)-z*1.007276466583,4)
        
        self.txt.set_text('%s\n%s\n%s' % (r'$\frac{m}{z}=$' + str(m_z),
                                                           r'$z=$'+str(z),
                                                           r'$mass(z)=$'+str(mass)))
        self.txt.set_position((m_z,y))
        
        self.ax.figure.canvas.draw_idle()