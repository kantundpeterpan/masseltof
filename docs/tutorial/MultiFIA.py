#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 18:03:52 2020

@author: kantundpeterpan
"""
import sys
sys.path.append('/media/kantundpeterpan/fast2/confinement/Python/')
from MassAnalyzer.MassWorkflows import MultiFileFIAAnalysis

files = [
 './data/bwd6_tetra/3110_BWD6LDT_T0_F17_BA2_01_5956.mzML',
 './data/bwd6_tetra/3110_BWD6LDT_T5_F15_BA4_01_5958.mzML',
 './data/bwd6_tetra/20200123_XP13II_20L_F13_RA4_01_6949.mzML',
 './data/bwd6_tetra/20200123_XP13II_40L_F18_RB1_01_6954.mzML',
 './data/bwd6_tetra/3110_BWD6LDT_T60_F10_BA6_01_5960.mzML']

#w.content()

z = 1

dbs = [['Tetra', 'Tetra hybrids hylg']] * len(files)

x1 = MultiFileFIAAnalysis(
        files,
        dbs,
        [str(i) for i in (0,5,20,40,60)], #sample identifier
        [(0.3, 0.4) for i in range(len(files))], #integration range
        z,
        remove_ambig=False,
        thres = 1e3
    )


#drop spiked peaks, keep most intense
x1 = x1.data.groupby(['sample', 'mz_calc'], as_index=False, group_keys=False)\
    .apply(lambda x:x.loc[x['y'].idxmax()])\
        .reset_index(drop=True)

x = x1.pivot_table(index='sample', columns = 'isotopologue', values='y').fillna(0)
x = x.div(x.sum(axis=1), axis=0)
x.index = [int(t) for t in x.index]
x = x.sort_index()
ax = x[['all heavy', 'all light', 'h2_hylg']].plot(marker = 'o')
ax.set_xlabel('Time (min)')
ax.set_ylabel('relative Abundance (%)')
