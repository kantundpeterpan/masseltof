Quickstart
##########

* `Installation`_
* `Loading mzML-Files`_
* `Combine scans into a mass spectrum`_
* `Peak detection`_
* `Visualizing detected peaks`_
* `Peak refinement`_
* `Peak matching against the MassElTOF database`_

Installation
************

For the time being MassElTOF has to installed more or less manually by cloning the repository and adding its path to the python packages. Using *conda* and the *env.yaml* an environment containing all necessary packages can be created.

.. code-block:: bash

   git clone https://gitlab.com/kantundpeterpan/masseltof.git ./masseltof
   cd masseltof
   conda env create -f env.yaml -n masseltof
   conda activate masseltof

.. code-block:: python

   import sys
   sys.path.append('/path/to/masseltof')

Loading mzML-Files
******************

.. code-block:: python

   from masseltof import MSanalyzer as msa
   x = msa(filename='./data/bwd6_tetra/3110_BWD6LDT_T0_F17_BA2_01_5956.mzML',
        plot_tic = True)

.. plot:: ../../tutorial/loading_data.py


Combine scans into a mass spectrum
**********************************

.. code-block:: python

   x.mzml_parser.combine_spectra(0.3, 0.4)
   x.analysis.plot_spectrum()

.. plot:: ../../tutorial/combine_spectra.py


PeakTools
*********

Peak detection
**************
Peak detection relies on functionality from the `peakutils <https://peakutils.readthedocs.io/en/latest/>`_ package.


.. code-block:: python

   import masseltof.PeakTools as pt

   #limiting peak detection to m/z between 970 and 990
   #using an absolute threshold of 0.1e6
   peaks = pt.PeakFinder.find_peaks(
                x.data.raw,
                mzmin = 970,
                mzmax = 990,
                thres_abs = True,
                thres = 0.15e6)

   #peaks is a pandas.DataFrame
   peaks
   
=========  =======  ================
  spec_id        x                 y
=========  =======  ================
   231790  984.512  220007
   232052  985.514  936212
   232183  986.016  169035
   232315  986.518  1829860
=========  =======  ================  

Visualizing detected peaks
**************************

.. code-block:: python

   x.analysis.ax.scatter(peaks.x, peaks.y)

.. plot:: ../../tutorial/pt_picking.py

Peak refinement
***************

The peaks detected by the *find_peaks* method are estimations based on the
1D-array of intensity values.
The peak locations can be refined by either fitting a Gaussian curve or determination of the peak centroid.

.. code-block:: python

   #calc_ppm to False as peaks have no compound assigned
   pt.PeakRefiner.centroid(peaks, x.data.raw, calc_ppm = False)
   pt.PeakRefiner.gaussian_fit(peaks, x.data.raw, calc_ppm = False)

   peaks

=========  ========  =======  ==========  =========
  spec_id         x        y    centroid    peakfit
=========  ========  =======  ==========  =========
   231790  984.5116  2.2E+05    984.5127   984.5112
   232052  985.5137  9.4E+05    985.5151   985.5143
   232183  986.0161  1.7E+05    986.0177   986.0166
   232315  986.5180  1.8E+06    986.5180   986.5169
=========  ========  =======  ==========  =========

Peak matching against the MassElTof database
*************************************************
A peaklist obtained as outlined above can be compared the MassElTOF database of compounds based on the deviation from the calculated mass.

.. code-block:: python

   found = pt.PeakIdentifier.identify(peaks, ['Tetra'],
                                      limit_ppm = 5)

   doctbl = found[['mz_obs', 'mz_calc', 'ppm',
                'labeling', 'isotopologue']]
   doctbl

========  =========  ======  ======================  ==============
  mz_obs    mz_calc     ppm  labeling                isotopologue
========  =========  ======  ======================  ==============
986.5180   986.5189  0.8996  [('C', 13), ('N', 15)]  all 13C and 15N
========  =========  ======  ======================  ==============

In this case, the detected peaks were matched against the all precalculated isotopologues of the
GlcNAc-MurNAc-Tetrapeptide. The peak at *986.5180* was identified as the uniformly labeled isotopologue
with a deviation of ca. 0.9 ppm from the calculated mass.
The :ref:`Setup of the MassElTOF database` is explained in an additional tutorial.

Peak matching against a custom m/z list
***************************************

TBD
