Setup of the MassElTOF database
===================================
Files for isotope, residue and species definitions are organized in the `data` folder of the MassElTOF package. Definitions are loaded from these files during package import. For the moment data are stored in flat *.csv* files.

Isotope definitions
*******************

The raw data of bundled isotope definitions were retrieved from the `NIST website <https://physics.nist.gov/cgi-bin/Compositions/stand_alone.pl>`_ and then processed accordingly.
The respective files can be found in the `data/IsotopeDef` folder.

==============  =======  ====================
element_symbol  isotope  isotopic_composition
==============  =======  ====================
H                 1              0.999885
==============  =======  ====================
...

The isotope data are used for the construction of the :ref:`IsoDB`, which provides convenient access to isotopic masses.

Residue definitions
*******************

MassElTOF allows arbitrary residue definitions based on their chemical composition.
The definitions are stored in a flat `.csv` file in the `data/ResidueDef` folder.
Residues are defined by a name and the elemental composition. In principle arbitrary
elemental compositions can be defined. However, tests have only been performed for the
elements shown in the table below.
Additional columns contain the monoisotopic masses of uniformly labeled residues according to the isotopes given in the column headers. These pre-calculated values are used for the calculation of
peptide masses. If the cell is left empty, the residues mass will be calculated accordingly when needed.
Beware, that this substantially slows down calculations in the current implementation.
*C12N14* denotes natural abundances.

=========  ===  ===  ===  ===  ===  ===  ========  ========
Residue      C    H    N    O    S    P    C12N14    C13N15
=========  ===  ===  ===  ===  ===  ===  ========  ========
Ala          3    5    1    1    0    0   71.0371   75.0442
=========  ===  ===  ===  ===  ===  ===  ========  ========

...

(monoisotopic masses have been rounded to 4 digits for clarity)

Species definitions
*******************

Residues as defined above, can be used to construct molecular species.
Definitions are stored in `.csv` files. The table for species definitions has
three columns: Species, sequence and labeling_variants.

Species are constructed from the residues as defined above. Bonds are indicated by a '+' (*e.g.* Ala+Glu). Modified residues are preceed by 
the modifier string followed by '*' (at the moment only 'C_ter' and 'N_ter' are supported; *e.g.* N_ter*Ala+Glu+C_ter*Ala). 

**CAVEAT** A major caveat is the inconsistency between species representation for the MS1 database and for calculations of fragmentation spectra. For the time being, only linear species can be represented in the MassElTOF database.
