.. MassElTOF documentation master file, created by
   sphinx-quickstart on Wed Jun  9 11:20:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MassElTOF's documentation!
=====================================

.. toctree::
   :maxdepth: 2

   quickstart
   Workflows
   ms1_database
   IsoDB
   modules

Introduction
############

This package started out as a tool for interactive annotation of peaks in
mass spectrometry data. It quickly became more than that and is an integral
part of the data processing and analysis pipeline for one of my PhD projects.
As such, this tool is heavily biased towards acquisition routines, files, data structures
and analysis needs of my research group.

Versatility and modular code have been emphasized especially for parts of
the package which have been written more recently. The code quality reflects rather
accurately my progress in programming principles - simply put: parts of the
code definitely need some polishing. There are inconsistencies between representation of chemical species in the database for the calculation of theoretical *m/z* values and the representation of species for calculations of fragmentation patterns.

Do not hesitate to ask questions or open an issue on GitLab or to contact me directly via mail (heiner.atze@gmx.net) or twitter (@kantundpeterpan).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
