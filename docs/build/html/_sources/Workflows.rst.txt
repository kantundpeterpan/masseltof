Workflows
=========

MassElTOF contains two classes, which represent Data analysis workflows - MultiFileFIAAnalysis and MS2Search.

MultiFileFIAAnalysis
********************

This class is used for targeted analysis of data from Multiple Flow Injections stored in several files. Theoretically it should be usable for LC/MS data as well (albeit untested).

.. code-block:: python

   from masseltof.MassWorkflows import MultiFileFIAAnalysis
   files = [
            './data/bwd6_tetra/3110_BWD6LDT_T0_F17_BA2_01_5956.mzML',
            './data/bwd6_tetra/3110_BWD6LDT_T5_F15_BA4_01_5958.mzML',
            './data/bwd6_tetra/20200123_XP13II_20L_F13_RA4_01_6949.mzML',
            './data/bwd6_tetra/20200123_XP13II_40L_F18_RB1_01_6954.mzML',
            './data/bwd6_tetra/3110_BWD6LDT_T60_F10_BA6_01_5960.mzML'
           ]


   z = 1 #setting charge for correct lookup in the MassElTOF database

   dbs = [['Tetra', 'Tetra hybrids hylg']] * len(files) #setting species to check against from the database for each file

   x1 = MultiFileFIAAnalysis(
                files,
                dbs,
                [str(i) for i in (0,5,20,40,60)], #sample identifier
                [(0.3, 0.4) for i in range(len(files))], #integration range
                z,
                remove_ambig=False,
                thres = 1e3
                )

   #some pandas juggling to drop spiked peaks, keep most intense
   x1 = x1.data.groupby(['sample', 'mz_calc'], as_index=False, group_keys=False)\
    .apply(lambda x:x.loc[x['y'].idxmax()])\
        .reset_index(drop=True)

   x1.head()

This will give x1, a pandas.DataFrame, with peak assignments, peak locations (peak maximum *mz_obs*, a gaussian fit *peakfit*, and the approximate peak centroid *centroid*), and deviations in ppm (for the peak maximum *ppm*, a gaussian fit *fit_ppm*, and the approximate peak centroid *centroid_ppm*)

=========  ========  =========  =========  ==============  ================  =========  =========  ==========  ==============  ========
  spec_id    mz_obs    mz_calc        ppm  isotopologue                   y    peakfit    fit_ppm    centroid    centroid_ppm    sample
=========  ========  =========  =========  ==============  ================  =========  =========  ==========  ==============  ========
   221054   942.413    942.416   3.03038   all light         1053              942.415  -0.366661     942.416        0.706642         0
   232789   986.517    986.519   2.08128   all heavy            1.82986e+06    986.517  -1.82594      986.518       -0.917892         0
   201208   942.416    942.416  -0.646866  all light            1.1151e+06     942.416   1.01857      942.417        1.58898         20
   202280   946.424    946.423  -1.46402   hAla_hylg       336347              946.425   2.94296      946.426        3.17947         20
   203081   949.433    949.433  -0.454565  h1_hylg         357402              949.434   1.25816      949.434        1.46476         20
=========  ========  =========  =========  ==============  ================  =========  =========  ==========  ==============  ========

These data can then be used in further analysis, *e.g.* calculation of relative abundances of the detected isotopologues in each sample.

.. code-block:: python

   x = x1.pivot_table(index='sample', columns = 'isotopologue', values='y').fillna(0)
   x = x.div(x.sum(axis=1), axis=0)
   x.index = [int(t) for t in x.index]
   x = x.sort_index()

   x

========  ===========  ===========  =========  ============  =========  ============  =========  ===========
  sample    all heavy    all light    h1_hylg    h2Hex_hylg    h2_hylg    h3Ala_hylg    h3_hylg    hAla_hylg
========  ===========  ===========  =========  ============  =========  ============  =========  ===========
       0        0.999        0.001      0.000         0.000      0.000         0.000      0.000        0.000
       5        0.859        0.058      0.010         0.012      0.007         0.007      0.039        0.008
      20        0.771        0.059      0.019         0.018      0.033         0.025      0.057        0.018
      40        0.551        0.207      0.041         0.024      0.066         0.029      0.045        0.037
      60        0.322        0.313      0.073         0.041      0.096         0.038      0.054        0.063
========  ===========  ===========  =========  ============  =========  ============  =========  ===========

.. code-block:: python

   x[['all heavy', 'all light', 'h2_hylg']].plot(marker = 'o')

.. plot:: ../../tutorial/MultiFIA.py


MS2Search
*********

This class can be used to annotate fragmentation spectra using previously calculated fragmentation patterns.

In order to annotate fragmentation spectra, we need to create theoretical peak lists as in the following example, which uses the dissaccharide-tetrapeptide peptide from *E. coli*. We suppose labeling with C13 and N15 in the  glucosamine portion of either GlcNAc or MurNAc.

**Note** that the syntax for species creation is different from species definition in the MassElTOF database. '-' indicates a bond and branches can be introduced by enclosing the respective residues in parentheses.
Please be also aware, that for the time being the fragmentation rules are hard-coded in the *masseltof.MS2Helper.Fragmenter* class and are applicable only within the scope of the muropeptide species predefined in the current MassElTOF database.

.. literalinclude:: ../../tutorial/MS2.py
   :language: python
   :lines: 11-58           

.. code-block:: python

   tetra_h1G.ms2.head()

The sequence of the fragments are indicated in the *frags* columns together with the *m/z* value for the mono(+)charged ion.

=======  =======================================================================================  =========  ====
    M_H  frags                                                                                      n_frags  id
=======  =======================================================================================  =========  ====
949.433  [('0!GlcN', '1!Ac', '2!GlcN_Red', '3!Ac', '4!Lac', '5!Ala', '6!Glu', '7!DAP', '8!Ala')]          1  h1G
860.385  [('0!GlcN', '1!Ac', '2!GlcN_Red', '3!Ac', '4!Lac', '5!Ala', '6!Glu', '7!DAP')]                   1  h1G
739.336  [('2!GlcN_Red', '3!Ac', '4!Lac', '5!Ala', '6!Glu', '7!DAP', '8!Ala')]                            1  h1G
688.3    [('0!GlcN', '1!Ac', '2!GlcN_Red', '3!Ac', '4!Lac', '5!Ala', '6!Glu')]                            1  h1G
650.288  [('2!GlcN_Red', '3!Ac', '4!Lac', '5!Ala', '6!Glu', '7!DAP')]                                     1  h1G
=======  =======================================================================================  =========  ====

Once theoretical fragmentation patterns have been calculated, we can load the experimental data.

.. literalinclude:: ../../tutorial/MS2.py
   :language: python
   :lines: 60-71

With the prepared data we can launch the MS2 workflow

.. literalinclude:: ../../tutorial/MS2.py
   :language: python
   :lines: 73-82

After completion the results have been stored in the *ms2* object.

When testing multiple hypotheses (given as first argument to the *MS2Search* class), common product ions corresponding to the same *m/z* value are extracted from the theoretical fragmentation patterns of the provided hypotheses.

In order to identify discriminatory product ions, each theoretical pattern is compared against the combined theoretical fragmentation patterns of all remaining possible solutions.

Please note that identification of chemcial species in tandom mass spectrometry can also be based on the detection of unique *m/z* transitions between precursor and product ions as well as between two product ions. For the time being, cannot analyze this kind of information.

.. code-block:: python

   ms2.found_ambig #pandas.DataFrame containing peaks that can be account for by at least two suggested species
   ms2.found_unique #list of pandas.DataFrames with peaks that could unambigously be attributed to one of the suggested species

The results can be compiled into an interactive report, which can be directly opened in any standard browser and/or be saved in an HTML-file.

.. literalinclude:: ../../tutorial/MS2.py
   :language: python
   :lines: 84-

.. raw:: html

   <iframe src="_static/tetra_h1_table_common.html" height=300px width="100%" frameBorder=0></iframe>

.. raw:: html

   <iframe src="_static/tetra_h1_table_disc.html" height=300px width="100%" frameBorder=0></iframe>

Non-discriminatory peaks are labeled with blue dots (common) and discriminatory peaks are automatically assigned different colors for each species. Clicking on the symbols in the legend hides the corresponding glyphs.

.. raw:: html

   <iframe src="_static/tetra_h1_plot.html" width="100%" height=550px frameBorder=0></iframe>
