import numpy as np
#pythran export binning(float64[:,:], float64)
def binning(mzs,
            binsize,
           ):

    min_mz = np.min(mzs[:,0])
    max_mz = np.max(mzs[:,0])
    
    delta = max_mz-min_mz
    
    bins = np.linspace(min_mz, max_mz,num = int(delta/binsize))
    
    spectrum = np.empty((len(bins),2))
    spectrum[:,0] = bins+binsize/2
    spectrum[:,1] = np.zeros(len(bins))
    
    chunked = np.array_split(mzs, indices_or_sections=100)
    
    for div in chunked:
        
        mz_s = div[:,0]
        counts = div[:,1].astype(int)

        ind_mz_indices = np.digitize(mz_s, bins)
        
        populated_bins = np.unique(ind_mz_indices)
        
        ind_mz_indices-=1
        populated_bins-=1
        
        temp_spectrum = np.empty((len(bins),2))
        temp_spectrum[:,0] = bins
        temp_spectrum[:,1] = np.zeros(len(bins))
        for bin_no in populated_bins:

            mz = bins[bin_no]+binsize/2
            ind = (ind_mz_indices == bin_no)

            temp_sum_counts = int(np.sum(counts[ind]))
            
            temp_spectrum[bin_no][1] = int(temp_sum_counts)
            
            temp_spectrum[:,0] = bins+binsize/2
        
        spectrum[:,1]+=temp_spectrum[:,1]

    return spectrum
