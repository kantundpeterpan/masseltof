#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:20:53 2019

@author: kantundpeterpan
"""

import os
import pandas as pd
import numpy as np

class MS_data():
    
    '''Stores the data for MSAnalyzer'''
    def __init__(self, msanalyzer):
        
        print(os.getcwd())
        
        self.msanalyzer = msanalyzer #reference to Classwrapper
        if ('.txt' in self.msanalyzer.path) or ('.csv' in self.msanalyzer.path):
            self.raw = msanalyzer.raw_data.copy() #copying data from MSAnalyzer
        
        columns = ['peak_label', 'm/z', 'z', 'mass', 'intensity','calc_mass', 'delta', 'remarks', 'ann_obj', 'ann_x', 'ann_y']
        
        if os.path.exists(self.msanalyzer.path[:-4] + '.csv') and not '.csv' in self.msanalyzer.path:
            self.peaks = pd.read_csv(self.msanalyzer.path[:-4] + '.csv', index_col = 0, header=0)
            self.peaks.index = np.round(self.peaks.index.values, 4)
        else:
            self.peaks = pd.DataFrame(columns = columns) #casting dataframe for storing annotated and commented peaks
            self.peaks = self.peaks.set_index('m/z')
            
        self.peaks.z = self.peaks.z.astype(int)
            
    def normalize(df,min_mass, max_mass):
        '''Can be used to normalize the whole data to one peak'''
        ind = (df.x>min_mass) & (df.x<max_mass)
        df['y_norm'] = df.y / df.y[ind].max()
