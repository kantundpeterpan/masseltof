#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 13:42:56 2020

@author: kantundpeterpan
"""

import peakutils as pu

class PeakRefiner():
    
    @classmethod
    def gaussian_fit(cls, peak_table, data,
                     data_mz_col = 'x',
                     data_int_col = 'y',
                     calc_ppm = True,
                     **pu_interp_kws):
        peak_table['peakfit'] = pu.interpolate(getattr(data, data_mz_col).values,
                                               getattr(data, data_int_col).values,
                                               ind=peak_table.spec_id,
                                               **pu_interp_kws)
        if calc_ppm:
            peak_table['fit_ppm'] = (peak_table.peakfit - peak_table.mz_calc)/peak_table.mz_calc * 1e6
        
        
    @classmethod
    def centroid(cls, peak_table, data,
                     data_mz_col = 'x',
                     data_int_col = 'y',
                     width = 10,
                     calc_ppm = True,
                     **pu_centroid_kws):
        
        tmp_centroids = []
        
        for idx in peak_table.spec_id:
            tmp = pu.centroid(getattr(data, data_mz_col)[idx-width:idx+width],
                              getattr(data, data_int_col)[idx-width:idx+width])
            
            tmp_centroids.append(tmp)
            
        peak_table['centroid'] = tmp_centroids
        if calc_ppm:
            peak_table['centroid_ppm'] = (peak_table.centroid - peak_table.mz_calc)/peak_table.mz_calc * 1e6
        
        