#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 15:57:46 2020

@author: kantundpeterpan
"""
import pandas as pd
import numpy as np
from scipy.spatial import distance_matrix

from .. import MSanalyzer

from tqdm import tqdm
import gc

class PeakIdentifier():

    @classmethod
    def distance(cls, mzs, species, z):
        #print(MSanalyzer.mol_db.columns)
        MSanalyzer(filename='')
                
        data = []
        
        ###!!!CHECK IF .APPLY CAN BE USED HERE
        for s in species:
            #print(s)#set_index('Species')
            db = MSanalyzer.mol_db.species_obj[s].labeling_variants.iloc[:,z]
            for spec_idx, mz_obs in mzs.to_frame().iterrows():
                mz_obs = float(mz_obs)
                for idx, mz_calc in db.to_frame().iterrows():
                    mz_calc = float(mz_calc)
                    delta = (mz_obs - mz_calc)/mz_calc*1e6
                    data.append((spec_idx, mz_obs, mz_calc,
                                 delta, s, idx))
                
        df = pd.DataFrame(data, columns=['spec_id','mz_obs', 'mz_calc', 'ppm',
                                         'murop', 'isotopologue'])
        
        return df
    
    @classmethod
    def distance_ms2(cls, obs, calc):
        data = []
        ###Extremely slow
        for spec_idx, mz_obs in tqdm(obs.to_frame().iterrows(),
                                     total=obs.to_frame().shape[0]):
            mz_obs = float(mz_obs)
            for idx, row in calc.iterrows():
                mz_calc = float(row.M_H)
                delta = (mz_obs - mz_calc)/mz_calc*1e6
                data.append((spec_idx, mz_obs, mz_calc,
                             delta, row.frags))
                
        df = pd.DataFrame(data, columns=['spec_id','mz_obs', 'mz_calc', 'ppm',
                                         'fragment'])
        
        return df
    
    @classmethod
    def distance_by_matrix(cls, obs, calc, #remove z argument !!
                           calc_mz_col = 'M_H'):
        
        calc.index.name = 'isotopologue'
        #dm = distance_matrix(obs.x.to_frame(), calc.M_H.to_frame())
        #ppm = np.divide(dm, calc.M_H.values)*1e6
        
        dm = np.subtract.outer(getattr(calc, calc_mz_col).values,
                               obs.x.values).transpose()
        
        ppm = pd.DataFrame(np.divide(dm, getattr(calc, calc_mz_col).values)*1e6,
                           columns = getattr(calc, calc_mz_col), index = obs.x.values)
        
        ppm.index.name = 'mz_obs'
        
        df = pd.melt(ppm.reset_index(), id_vars='mz_obs', value_vars=ppm.columns, 
                     value_name = 'ppm', var_name='mz_calc')
        
        obs.index.name = 'spec_id'
        
        #df = df.set_index('mz_obs')
        #obs = obs.set_index('x')

        df = pd.merge(df, obs, left_on='mz_obs', right_on='x').drop('x', axis=1).astype(float)

        #df = pd.merge(df, obs, left_index=True, right_index=True)
        #df.index.name = 'mz_obs'
        #df = df.reset_index()
        #df = df.set_index('mz_calc')
        
        #calc = calc.set_index('M_H', drop=False)
        
        df = pd.merge(df, calc.reset_index(), left_on='mz_calc', right_on=calc_mz_col).drop(calc_mz_col, axis=1)
        #df = pd.merge(df, calc, left_index=True, right_index=True)#.drop('M_H', axis=1)
        
        #gc.collect()
        
        return df  
        
    @classmethod
    def identify_ms2(cls, mzs, calc, limit_ppm=5,
                 remove_ambig=False,
                 keep='max',
                 crit_col='y'):
        
        keep_func_map = {
                'max':'idxmax',
                'min':'idxmin'
            }
        
        dist = cls.distance_by_matrix(mzs, calc)
        ind = abs(dist.ppm) <= limit_ppm
        found = dist.loc[ind,:]
        
        print(found.head())

        print(remove_ambig)
        if remove_ambig:
            found = found.groupby('mz_calc', as_index=False, group_keys=False)\
                    .apply(lambda x:x.loc[
                        getattr(getattr(x, crit_col), keep_func_map[keep])()
                        ])
            if found.empty:
                found = pd.DataFrame(
                        columns = ['mz_obs', 'mz_calc', 'ppm', 'spec_id', 'y', 'frags', 'n_frags', 'id']
                    )

        gc.collect()
        
        print(found.head())
        return found
            
    @classmethod
    def identify(cls, mzs, species, limit_ppm=5,
                 remove_ambig=False,
                 keep='max',
                 crit_col='y',
                 calc_mz_col = 'M_H'
                 ):
        
        keep_func_map = {
                'max':'idxmax',
                'min':'idxmin'
            }
        
        #dist = cls.distance(mzs, species, z)
        
        #new
        species = MSanalyzer.mol_db.species_obj[species].values
        #print(species)
        dist = pd.concat([cls.distance_by_matrix(mzs, s.labeling_variants, calc_mz_col = calc_mz_col) for s in species])
        ###

        ind = abs(dist.ppm) <= limit_ppm
        found = dist.loc[ind,:]
        #return found
        ambig = (found.isotopologue.value_counts()>1).sum() > 0
       
        '''
        if remove_ambig:
            found = found.groupby('mz_calc', as_index=False, group_keys=False)\
                    .apply(lambda x:x.loc[
                        getattr(getattr(x, crit_col), keep_func_map[keep])()
                        ])data.x
        
        gc.collect()
            
        return found
        '''
        if ambig:
             print('### !!! ambiguous results !!! ')
             
        if ambig and remove_ambig:
            #find peak with multiple matches
            ambig_ind = found.isotopologue.value_counts()>1
            ambig_isotop = ambig_ind.index.to_list()
            
            #get rid off all multiple matched peaks
            while ambig:
                
                try:
                    print(found)
                    
                    to_drop = input('Choose peak(s) to drop: ')
                    to_drop = [int(i) for i in to_drop.split(',')]
                    
                except KeyboardInterrupt:
                    break
                
                except:
                    print('### Please check input')
                    continue
                
                found = found.drop(to_drop)
                
                ambig = (found.isotopologue.value_counts()>1).sum() > 0            
        
        return found
    
    @classmethod
    def identify_np(cls,
                    peaks, species,
                    limit_ppm, db,
                    species_col = 'murop',
                    peak_mz_col = 'x',
                    db_mz_col = 'M_H'
                    ):
        '''
        peak annotation using a table of known, annotated masses
        against a simple (x,y) peaklist
        '''
        if not species == 'all':
            big = db.loc[np.in1d(getattr(db, species_col), species)]
        else:
            big = db
        
        s = np.subtract.outer(
            getattr(big, db_mz_col).values,
            getattr(peaks, peak_mz_col).values
                              )
        
        d = np.divide(s, getattr(big, db_mz_col).values.reshape(-1,1))*1000000
        
        idx = np.abs(d) <= limit_ppm
        
        found = np.where(idx==1)
        
        df = pd.DataFrame(
            np.append(big.iloc[found[0]], peaks.iloc[found[-1]], axis=1),
            columns = big.columns.to_list() + peaks.columns.to_list()
            )
        
        df['ppm'] = d[idx].ravel()
        
        return df
