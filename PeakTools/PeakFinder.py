#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 15:42:53 2020

@author: kantundpeterpan
"""

import peakutils as pu
from scipy.signal import savgol_filter

class PeakFinder():
    
    @classmethod
    def find_peaks(cls, spec, mzmin, mzmax,
                   savitzky_golay=False,
                   spec_mz_col = 'x',
                   spec_int_col = 'y',
                   **kws):
        
        ind = (getattr(spec, spec_mz_col) >= mzmin) & (getattr(spec, spec_mz_col) <= mzmax)
        tmp_spec = spec.loc[ind,:]
        tmp_spec.index.name = 'spec_id'
        
        if savitzky_golay:
            idx = pu.indexes(
                    savgol_filter(
                            getattr(tmp_spec, spec_int_col),
                            kws.pop('sg_window', 11),
                            kws.pop('sg_polyorder', 5)
                        ),
                    **kws,
                )
        else:            
            idx = pu.indexes(getattr(tmp_spec, spec_int_col), **kws)
        
        peaks = tmp_spec.iloc[idx,:].reset_index()
        peaks.spec_id = peaks.spec_id.astype(int)
        
        return peaks
        
