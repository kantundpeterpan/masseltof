#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 15:22:58 2020

@author: kantundpeterpan
"""

#(mzml_file, dbs_to_search_timepoint)

from MSanalyzer_class import MSanalyzer
from PeakFinder import PeakFinder
from PeakIdentifier import PeakIdentifier
from PeakRefiner import PeakRefiner

import pandas as pd

class MultiFileFIAAnalysis():
    
    def __init__(self, files, dbs_to_search, sample_identifiers,
                 int_ranges, z, mzmin=800, mzmax = 1000, verbose=True,
                 remove_ambig=False):
        
        data = []
        
        for file, dbs, si, ir in zip(files, dbs_to_search,
                                     sample_identifiers, int_ranges):
            tmp = MSanalyzer(filename=file,
                             plot_tic=False,
                             fast=True,
                             fast_stream=True)
            
            tmp.mzml_parser.combine_spectra_iter(*ir)
            
            tmp_p = PeakFinder.find_peaks(tmp.data.raw, mzmin, mzmax, thres=0.001)
            tmp_found = PeakIdentifier.identify(tmp_p.x, dbs, z, limit_ppm=10,
                                                remove_ambig=remove_ambig)
            
            PeakRefiner.gaussian_fit(tmp_found, tmp.data.raw)
            PeakRefiner.centroid(tmp_found, tmp.data.raw)
            
            tmp_found = pd.merge(tmp_found, tmp_p,
                                 left_on='mz_obs', right_on='x').drop('x', axis=1)
            
            tmp_found['sample'] = si
            
            if verbose:
                print("####", si)
                print(tmp_found[['isotopologue', 'y']])
            
            data.append(tmp_found)
        
        self.data = pd.concat(data)