#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 15:10:15 2020

@author: kantundpeterpan
"""

import os
import pandas as pd
#from .Species import Re
from .LabelDict import LabelDict

class ResidueDB():
    
    labelings = ['C12N14',
                 'C13N15',
                 'C12N15',
                 'C13N14']
    
    ResDefFiles = ['residues.csv']
    PreCalcFile = 'ResidueMasses.pkl' #pandas DataFrame, index=Residues, columns=labelings
    #residues definitions
    #precalculated residue masses for arbitrary isotope compositions
    
    #load residue definition
    #check if all residues have precalculated masses for all requested isotopic combinations