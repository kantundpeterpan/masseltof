#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:15:04 2019

@author: kantundpeterpan
"""

import sys

from MSanalyzer_class import *
from test_file_dialog import *
from PyQt5 import QtGui, QtCore, QtWidgets
from IPython import embed

class Window(QtWidgets.QDialog):

    def __setattr__(self, name, value):
        self.__dict__[name] = value
        
        if name == 'msanalyzer' and value == None:
            self.plot_button.setDisabled(True)
            self.load_peaks_button.setDisabled(True)

    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        
        self.filedialog = FileDialog()
        
        self.setGeometry(50, 50, 50, 50)
        self.setWindowTitle("MSanalyzer Home")
        
        self.layout = QtWidgets.QFormLayout(self)
        
        self.menu = QtWidgets.QGroupBox('Menu')
        
        #setting layout
        self.menu_layout = QtWidgets.QGridLayout(self)
        
        #setting widgets
        self.load_button = QtWidgets.QPushButton('Load')
        self.load_button.clicked.connect(self.load_analyzer)
        
        self.plot_button = QtWidgets.QPushButton('Plot')
        self.plot_button.clicked.connect(self.plot_click) 
        
        self.load_peaks_button = QtWidgets.QPushButton('Load peak annotations')
        self.load_peaks_button.clicked.connect(self.load_peaks)
        
        self.ipython_button = QtWidgets.QPushButton('Mess with me!')
        self.ipython_button.clicked.connect(embed)
        
        #self.menu_layout.setColumnStretch(0,1)
        self.menu_layout.addWidget(self.load_button, 0, 0)
        self.menu_layout.addWidget(self.plot_button, 1, 0)
        self.menu_layout.addWidget(self.load_peaks_button, 2, 0)
        self.menu_layout.addWidget(self.ipython_button, 3, 0)
        self.menu.setLayout(self.menu_layout)
        
        self.layout.addWidget(self.menu)
        #self.setLayout(self.layout)
        
        self.msanalyzer = MSanalyzer()
        
        self.show()

    def load_analyzer(self):
        filename = self.filedialog.openFileNameDialog()
        
        self.msanalyzer = None
        self.msanalyzer = MSanalyzer(filename)
        
        self.plot_button.setDisabled(False)
        self.load_peaks_button.setDisabled(False)
        
        #self.msanalyzer.analysis.plot_spectrum()
        
    def load_peaks(self):
        filename = self.filedialog.openFileNameDialog()
        
        self.msanalyzer.data.peaks = pd.read_csv(filename, index_col = 0, header=0)
        self.msanalyzer.data.peaks.index = np.round(self.msanalyzer.data.peaks.index.values, 4)
        
    def plot_click(self,**args):
        self.msanalyzer.analysis.plot_spectrum(**args)

#if __name__ == "__main__":        
app = QtWidgets.QApplication(sys.argv)


GUI = Window()
sys.exit(app.exec_())
