#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 15:23:15 2020

@author: kantundpeterpan
"""

import enum
import pandas as pd
import os
import types
from functools import partial

class GetItem(type):
    def __getitem__(cls, name):
        return getattr(cls, name)

class IsoDB(metaclass = GetItem):
    
    path = os.path.dirname(__file__)
    
    isodf = pd.read_csv(os.path.join(path, 'data', 'IsotopeDef',
                                     'isotopes_atomic_masses_processed.csv'))
    
    mostabundant = isodf.dropna().set_index('isotope').groupby('element_symbol')['isotopic_composition'].idxmax().astype(int)
    
    @classmethod
    def isotopize(cls):
        groups = cls.isodf.set_index('isotope').groupby('element_symbol')[['element_symbol', 'atomic_mass']]
        _mapfunc = lambda x: setattr(cls, x.element_symbol.unique()[0],
                                     x['atomic_mass'].to_dict())
        groups.apply(_mapfunc)