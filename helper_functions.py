#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 18:22:47 2019

@author: kantundpeterpan
"""

import pandas as pd

def load_mz_txt(file):
    df = pd.read_csv(file, sep = ' ', header = None)
    df.columns = ['x','y']
    return df