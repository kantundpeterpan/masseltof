#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 11:39:32 2021

@author: kantundpeterpan
"""
import os
import sys
sys.path.append('/media/kantundpeterpan/fast2/csonfinement/Python/')
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from MassAnalyzer import MSanalyzer
import os
#x = MSanalyzer(fast=True, fast_stream=True, plot_tic=False)
#x.mzml_parser.combine_spectra_iter(0.6, 0.72)

#calib = x.data.raw

directory = os.path.abspath(os.path.dirname(__file__))

formate = pd.read_csv(os.path.abspath(directory + '/Na_formate_openms.csv'),
                                      names = ['M_H', 'bla', 'blu'])

from MassAnalyzer.PeakTools import PeakIdentifier, PeakFinder

def calibrate(ms, int_range=(0.6, 0.72)):
    ms.mzml_parser.combine_spectra_iter(*int_range)
    calib = ms.data.raw
    p = PeakFinder.find_peaks(calib, 0, 2000, thres_abs=True, thres = 1e4)
    m = PeakIdentifier.distance_by_matrix(p, formate)
    m = m.loc[m.ppm.abs()<30]
    m = m.groupby('mz_calc').apply(lambda x: x.loc[x.y.idxmax()])
    s, i = np.polyfit(m.mz_obs, m.mz_calc, 1)
    
    return s, i
'''
no_sections = 1
#def run(no_sections):
p = PeakFinder.find_peaks(calib, 0, 2000, thres_abs=True, thres = 1e4)

m = PeakIdentifier.distance_by_matrix(p, formate)
m = m.loc[m.ppm.abs()<30]
m = m.groupby('mz_calc').apply(lambda x: x.loc[x.y.idxmax()])

ranges = []
p_new = p.copy()
#get split indices
idx = np.array_split(np.arange(m.shape[0]), no_sections)
for chunk in idx:
    s1, i = np.polyfit(m.iloc[chunk].mz_obs, m.iloc[chunk].mz_calc, 1)
    print(s1, i)
    ind = (p_new.x >= m.iloc[chunk].mz_obs.min()) & (p_new.x <= m.iloc[chunk].mz_obs.max())
    tmp_x = p_new.x.loc[ind]
    tmp_x = tmp_x*s1+i
    p_new.x.loc[ind] = tmp_x.values
    ranges.append((m.iloc[chunk].mz_obs.min(), m.iloc[chunk].mz_obs.max(), s1, i))

#p_new.x = p_new.x*s+i
#p_new.x = p_new.x**3*s1+p_new.x**2*s2+p_new.x*s3+i

m_new = PeakIdentifier.distance_by_matrix(p_new, formate)
m_new = m_new.loc[m_new.ppm.abs()<30]
m_new = m_new.groupby('mz_calc').apply(lambda x: x.loc[x.y.idxmax()])

print('#### %s'% no_sections)
print(m.ppm.min().round(2), m.ppm.mean().round(2), m.ppm.max().round(2))
print(m_new.ppm.min().round(2), m_new.ppm.mean().round(2), m_new.ppm.abs().max().round(2))

#plt.scatter(m.mz_obs, m.ppm)
#plt.scatter(m_new.mz_obs, m_new.ppm)

x.mzml_parser.combine_spectra_iter(0.1, 0.5)
pm = PeakFinder.find_peaks(x.data.raw, 930, 980, thres_abs=True, thres = 1e3)
xm = PeakIdentifier.identify(pm.x, ['Tetra-Tetra', 'Tetra-Tetra hybrids hylg'], 2, limit_ppm=15)
print(xm.ppm.min().round(2), xm.ppm.mean().round(2), xm.ppm.max().round(2))
plt.scatter(xm.mz_obs, xm.ppm)

#pm.x = pm.x*ranges[0][-2]+ranges[0][-1]
for r in ranges:
    xmin, xmax, s, i = r
    ind = (pm.x >= xmin) & (pm.x <= xmax)
    pm.x.loc[ind] = pm.x.loc[ind]*s+i
xm = PeakIdentifier.identify(pm.x, ['Tetra-Tetra', 'Tetra-Tetra hybrids hylg'], 2, limit_ppm=15)
print(xm.ppm.min().round(2), xm.ppm.mean().round(2), xm.ppm.max().round(2))
plt.scatter(xm.mz_obs, xm.ppm)
'''