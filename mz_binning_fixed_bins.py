import numpy as np
#pythran export binning_fixed(float64[:,:], float64[:], float64)
def binning_fixed(mzs, bins, binsize):
      
    bin_chunks = np.array_split(bins, 100)
    
    spectrum = []
    
    for i,bin_chunk in enumerate(bin_chunks):
        
        min_bin = np.min(bin_chunk)
        max_bin = np.max(bin_chunk)        
        
        ind = np.logical_and(mzs[:,0]>=min_bin,mzs[:,0]<max_bin)

        
        if sum(ind)>=1:

            mz_s = mzs[:,0][ind]
            counts =mzs[:,1][ind].astype(int)

            ind_mz_indices = np.digitize(mz_s, bin_chunk)

            populated_bins = np.unique(ind_mz_indices)

            ind_mz_indices-=1
            populated_bins-=1

            temp_spectrum = np.empty((len(bin_chunk),2))
            temp_spectrum[:,0] = bin_chunk
            temp_spectrum[:,1] = np.zeros(len(bin_chunk))

            for bin_no in populated_bins:

                mz = bin_chunk[bin_no]+binsize/2
                ind = (ind_mz_indices == bin_no)

                temp_sum_counts = int(np.sum(counts[ind]))
                temp_spectrum[bin_no][1] = int(temp_sum_counts)
                temp_spectrum[:,0] = bin_chunk+binsize/2
                
            spectrum.append(temp_spectrum)
            
        else:
            next
            
        spectrum_stacked = np.vstack(spectrum)
    
    return spectrum_stacked