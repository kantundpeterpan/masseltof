# MassElTOF
---
MassElTOF is a python package for the analysis of mass spectrometry data. 
It was created as a custom analysis package for one of my PhD projects and is 
still a work in progress. 

A distinctive feature is its capability to calulate masses and theoretical fragmentation
 spectra of arbitrarily isotopically labeled peptides.

The (WIP) documentation can be found [here](https://kantundpeterpan.gitlab.io/masseltof).

This packages was used for the bigger of the analyses in the following publication:

**Atze, H.; Rusconi, F.; Arthur, M.**
Heavy Isotope Labeling and Mass Spectrometry Reveal Unexpected Remodeling of Bacterial Cell Wall Expansion in Response to Drugs. bioRxiv 2021. https://doi.org/10.1101/2021.08.06.454924.
