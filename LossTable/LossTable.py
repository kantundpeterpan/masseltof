#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 15:34:00 2020

@author: kantundpeterpan
"""
from scipy.spatial import distance_matrix
from MassAnalyzer import MSanalyzer

import pandas as pd

from numpy import np

"""
y =  MS2Search ...
"""

peaks = y.peaks.x.sort_values(ascending=False).to_frame()

d = distance_matrix(peaks,peaks)
df = pd.DataFrame(d, index = peaks, columns = peaks)

aa = MSanalyzer.residues['C12N14'] #supply precalculated losses, AA, Bases, Dipeptides, etc..

tst = [
       np.abs(
           np.divide.outer(
               np.subtract.outer(
                   df.values,x
                   )*1e6,
               x)
           )
       for x in aa
       ]


limit_ppm = 20
tst_ind = [x<limit_ppm for x in tst]

### How to check for ambigous losses?
for res, ind_m in zip(aa.index, tst_ind):
    aaa[ind_m] = res
    
### Calculate dimers of residues
newr = MSanalyzer.residues.dropna().reset_index()
fast = np.add.outer(newr['C12N14'].values, newr['C12N14'].values)
dipe = pd.DataFrame(fast, index = newr.Residue, columns = newr.Residue)
dipe.columns.name = 'Residue2'
dipe = pd.melt(dipe.reset_index(), id_vars=['Residue'])
dipe.index = dipe.Residue.str.cat(dipe.Residue2, sep='-')
dipe = dipe.drop(['Residue', 'Residue2'],axis=1)
dipe = dipe.drop_duplicates()
dipe = pd.Series(dipe.values.reshape(-1,), index = dipe.index)

fast = np.add.outer(dipe.values, newr['C12N14'].values)
tripe = pd.DataFrame(fast, index = dipe.index, columns=newr.Residue)
tripe.columns.name = 'Residue2'
tripe = pd.melt(tripe.reset_index(), id_vars=['Residue'])
tripe.index = tripe.Residue.str.cat(tripe.Residue2, sep='-')
tripe = tripe.drop(['Residue', 'Residue2'],axis=1)
tripe = tripe.drop_duplicates()
tripe = pd.Series(tripe.values.reshape(-1,), index = tripe.index)

newr['Residue2'] = newr.Residue
blal = pd.pivot_table(newr, index = 'Residue',
                      columns = 'Residue2',
                      values = 'C12N14', aggfunc='sum')
for idx, row in blal.iterrows():
    for res in row.index:
        blal.loc[idx, res] = MSanalyzer.residues.loc[idx, 'C12N14'] + MSanalyzer.residues.loc[res, 'C12N14']
