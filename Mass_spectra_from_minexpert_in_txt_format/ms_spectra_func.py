import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("Qt5Agg")
import numpy as np

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt

def annotate_peak(text, mass, mz,df, ax, arrowstyle = '-|>'):
    ind = (df.x > mass - 0.05) & (df.x < mass + 0.05)
    y = df.y_norm[ind].max()
    x = df.x[df.y_norm == y].values[0]
    y = y #* 1.01
    an = ax.annotate(s = '%s\n%s\n%s' % (text, r'$\frac{m}{z}=$' + str(x), r'$z=$'+str(mz)), xy = (x, y), xycoords = 'data', xytext = (2,2), textcoords = 'offset points', ha = 'left', va = 'top', arrowprops = {'arrowstyle':arrowstyle})
    an.draggable()

def label_on_click(event):
    if event.button == 1 and event.key == 'ctrl+L':
        mass = event.xdata 
        label = 'test' #input('Peak label: ')
        mz = '2' #input('m/z: ')
        ax = event.inaxes
        data = pd.DataFrame(np.array(ax.lines[0].get_data())).transpose()
        data.columns = ['x', 'y_norm']
        annotate_peak(label, mass, mz, data, ax) 
	

def normalize(df,min_mass, max_mass):
    ind = (df.x>min_mass) & (df.x<max_mass)
    df['y_norm'] = df.y / df.y[ind].max()
    
def load_mz_txt(file):
    df = pd.read_csv(file, sep = ' ', header = None)
    df.columns = ['x','y']
    return df

def plot_norm_ms(df, **kwargs):

    fig, ax = plt.subplots(1,1)
    fig.canvas.mpl_connect('button_press_event', label_on_click)

    root = fig.canvas.manager.window
    panel = QtWidgets.QWidget()
    hbox = QtWidgets.QHBoxLayout(panel)
    textbox = QtWidgets.QLineEdit(parent = panel)
#    textbox.textChanged.
    hbox.addWidget(textbox)
    panel.setLayout(hbox)

    dock = QtWidgets.QDockWidget("peak label", root)
    root.addDockWidget(Qt.BottomDockWidgetArea, dock)
    dock.setWidget(panel)

    plt.plot(df.x, df.y_norm, **kwargs)
    return fig, ax

class MSanalyzer():

	def __init__(self, filename):
		self.raw_data = load_mz_txt(filename)
