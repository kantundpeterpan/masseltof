#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import matplotlib
#matplotlib.rcParams['text.usetex'] = True


# In[2]:


sys.path.append('/home/kantundpeterpan/my_stuff_on_server/Python/')


# In[3]:


sys.path.append('/home/kantundpeterpan/my_stuff_on_server/Python/Isofit/')


# In[4]:


from MassAnalyzer import MSanalyzer_class


# In[5]:


import MassSpectrum


# In[6]:


import Isofit


# In[7]:


import numpy as np

import IPython
ipython = IPython.get_ipython()
#try:
ipython.magic('matplotlib qt')
#except:
#	pass

from ipywidgets import widgets, interact, interactive, fixed, interact_manual, Layout, Button, Box


# In[16]:


import matplotlib.pyplot as plt

#define Species - Tri-peptide
atom_counts = (59,20,6,0,34,0)
z=1

# In[19]:


c12 = MSanalyzer_class.MSanalyzer.isotope_masses.loc['C'].isotopic_composition.values[0]


# In[20]:


n14 = MSanalyzer_class.MSanalyzer.isotope_masses.loc['N'].isotopic_composition.values[0]


# In[21]:


#c12n14 - natural
confs, px, py, c12n14_centroid = Isofit.simulate_peaks((c12,n14), atom_counts, 4, return_sim_spec=True)


# In[22]:


c12n14_centroid[:,1] = c12n14_centroid[:,1]/np.max(c12n14_centroid[:,1])


# In[23]:


c12n14_centroid = MassSpectrum.MassSpectrum(c12n14_centroid, Isospec_input=True)


# In[24]:

fig, ax = plt.subplots(figsize=(18,8))
c12n14_centroid.plot_centroids(ax)

def test(c12=c12, n14=n14):

    confs, px, py, temp_centroid = Isofit.simulate_peaks((c12,n14), atom_counts, 4, return_sim_spec=True, z=z)
    
    temp_shaped = Isofit.peak_shaper(temp_centroid,
                                     40000,
                                     normalize=True,
                                     process_binning=True,
                                     binsize=0.003,
                                     no_points=600)
    
    temp_shaped = MassSpectrum.MassSpectrum(temp_shaped)
    
    x,y  = temp_shaped.mz, temp_shaped.ints

    temp_centroid[:,1] = temp_centroid[:,1]/np.max(temp_centroid[:,1])
    temp_centroid = MassSpectrum.MassSpectrum(temp_centroid, Isospec_input=True)   
    
    old_xlim = ax.get_xlim()
    old_ylim = ax.get_ylim()

    ax.clear()

    if plot_spectrum.value:
        temp_shaped.plot_spectrum(ax)
    if plot_centroids.value:
        temp_centroid.plot_centroids(ax)
    if plot_reference.value:
        reference.plot_spectrum(ax, linewidth=1, linestyle='--')
    if keep_x.value:
        ax.set_xlim(old_xlim)
    if keep_y.value:
        ax.set_ylim(old_ylim)

    ax.figure.canvas.draw()

    return c12, n14, confs


# In[25]:


c12_slider=widgets.FloatSlider(min=0, max=0.9999, step=0.0001, value=c12,
                      layout=Layout(width='66%', height='80px'))


# In[26]:


n14_slider=widgets.FloatSlider(min=0.0000, max=0.9999, step=0.0001, value=n14,
                      layout=Layout(width='66%', height='80px'))


# In[27]:


keep_x = widgets.Checkbox(description='keep x')


# In[28]:


keep_y = widgets.Checkbox(description='keep y')


# In[29]:


check_plot_elements_func = lambda x:test(c12_slider.value, n14_slider.value)


# In[30]:


plot_centroids = widgets.Checkbox(description='plot centroids', value=True)


# In[31]:


plot_centroids.observe(check_plot_elements_func, names='value')


# In[32]:


plot_spectrum = widgets.Checkbox(description='plot spectrum')


# In[33]:


plot_spectrum.observe(check_plot_elements_func, names='value')


# In[34]:


plot_reference = widgets.Checkbox(description='plot reference')


# In[35]:


plot_reference.observe(check_plot_elements_func, names='value')


# In[36]:


c12_func = lambda x:test(x['new'],n14_slider.value)


# In[37]:


c12_slider.observe(c12_func, names='value')


# In[38]:


n14_func = lambda x:test(c12_slider.value,x['new'])



# In[39]:


n14_slider.observe(n14_func, names='value')

text_c12 = widgets.Label(value = 'Rel. abundance C12')
text_box_c12 = widgets.FloatText(value = str(c12_slider.value), step=0.0001, continuous_update=False)
text_n14 = widgets.Label(value = 'Rel. abundance N14')
text_box_n14 = widgets.FloatText(value = str(n14_slider.value), step=0.0001, continous_update=False)

slider_to_text_box_dict = {c12_slider:text_box_c12, n14_slider:text_box_n14}
text_box_to_slider_dict = {val:key for key,val in slider_to_text_box_dict.items()}

def slider_value_to_textbox(x):
	slider_to_text_box_dict[x['owner']].value = x['new']
def textbox_value_to_slider(x):
	text_box_to_slider_dict[x['owner']].value = x['new']

c12_slider.observe(slider_value_to_textbox, names='value')
n14_slider.observe(slider_value_to_textbox, names='value')

text_box_c12.observe(textbox_value_to_slider, names='value')
text_box_n14.observe(textbox_value_to_slider, names='value')

reset_button = widgets.Button(description = 'Reset')

def reset_button_func(x):
    c12_slider.value = c12
    n14_slider.value = n14

reset_button.on_click(reset_button_func)

# In[40]:


items1 = [
    keep_x,
    keep_y,
    plot_centroids,
    plot_spectrum,
    plot_reference
 ]

box_layout1 = Layout(display='flex',
                    flex_flow='Column',
                    align_items='stretch',
                    border='None',
                    width='100%')
box1 = Box(children=items1, layout=box_layout1)


# In[41]:


items2 = [
    widgets.Label(value='Relative abundance C12'),
    c12_slider,
    widgets.Label(value='Relative abundance N14'),
    n14_slider
 ]

box_layout2 = Layout(display='flex',
                    flex_flow='Column',
                    align_items='stretch',
                    border='None',
                    width='100%')
box2 = Box(children=items2, layout=box_layout2)

items3 = [
    text_c12,
    text_box_c12,
    text_n14,
    text_box_n14,
    reset_button
 ]

box_layout3 = Layout(display='flex',
                    flex_flow='Column',
                    align_items='stretch',
                    border='None',
                    width='100%')
box3 = Box(children=items3, layout=box_layout3)
# In[42]:


total_items = [
    box1,
    box3,
    box2
]

big_box_layout = Layout(display='flex',
                    flex_flow='Row',
                    align_items='stretch',
                    border='None',
                    width='100%')

total_box = Box(children=total_items, layout = big_box_layout)

#try:
IPython.display.display(total_box)
#except:
#	pass




