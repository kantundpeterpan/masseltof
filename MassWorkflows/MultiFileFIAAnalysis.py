#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 11:39:02 2020

@author: kantundpeterpan
"""

from .. import MSanalyzer
from ..PeakTools import PeakFinder, PeakIdentifier, PeakRefiner
from ..calibration import calibrate

import pandas as pd


class MultiFileFIAAnalysis():
    
    def __init__(self, files, dbs_to_search, sample_identifiers,
                 int_ranges, z, mzmin=800, mzmax = 1000, verbose=True,
                 limit_ppm = 10,
                 remove_ambig=False,
                 calibrate_mz = False,
                 c_int_range = None,
                 **kwargs):
        
        data = []
        binsize = None
        if 'binsize' in kwargs.keys():
            binsize = kwargs.pop('binsize')

        thres = kwargs.pop('thres', 500)
        
        for file, dbs, si, ir in zip(files, dbs_to_search,
                                     sample_identifiers, int_ranges):
            tmp = MSanalyzer(filename=file,
                             plot_tic=False,
                             fast=True,
                             fast_stream=True)
            
            if calibrate_mz:
                s, intcp = calibrate(tmp, c_int_range)
            
            if binsize:
                tmp.mzml_parser.combine_spectra_iter(*ir, binsize = binsize)
            else:
                tmp.mzml_parser.combine_spectra_iter(*ir)
                
            if calibrate_mz:
                tmp.data.raw.x = tmp.data.raw.x*s + intcp

            tmp_p = PeakFinder.find_peaks(tmp.data.raw,
                                          mzmin, mzmax,
                                          thres_abs=True,
                                          thres=thres)

            mz_col = {1:'M_H', 2:'M_2H'}[z]
            
            tmp_found = PeakIdentifier.identify(tmp_p, dbs,
                                                limit_ppm=limit_ppm,
                                                remove_ambig=remove_ambig,
                                                calc_mz_col=mz_col)
            
            tmp_found.spec_id = tmp_found.spec_id.astype(int)

            PeakRefiner.gaussian_fit(tmp_found, tmp.data.raw,
                                     data_mz_col = 'x')
            PeakRefiner.centroid(tmp_found, tmp.data.raw,
                                 data_mz_col = 'x')
            
            
            tmp_found['sample'] = si
            
            if verbose:
                print("####", si)
                print(tmp_found[['isotopologue', 'y']])
            
            data.append(tmp_found)
        
        self.data = pd.concat(data)[['spec_id', 'mz_obs', 'mz_calc', 'ppm',
                                     'isotopologue', 'y', 'peakfit', 'fit_ppm', 
                                     'centroid', 'centroid_ppm', 'sample']]
