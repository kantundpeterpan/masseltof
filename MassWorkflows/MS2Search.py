#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 15:22:58 2020

@author: kantundpeterpan
"""

#(mzml_file, dbs_to_search_timepoint)

from .. import MSanalyzer
from ..PeakTools import PeakFinder, PeakIdentifier, PeakRefiner
from ..MS2Helper import Discriminator

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

class MS2Search():
    
    def __init__(self,
                 isotopomers,
                 int_range,
                 mzmin = 0,
                 mzmax = 2000,
                 file='filedialog',
                 limit_ppm=5,
                 thres=0.01,
                 thres_abs=False,
                 savitzky_golay = False,
                 ms = None,
                 **kws
                 ):
        
        if ms == None:
            ms = MSanalyzer(filename=file,
                            plot_tic=False,
                            fast=True,
                            fast_stream=True,
                            )
        
        if not hasattr(ms.data, 'raw'):
            ms.mzml_parser.combine_spectra_iter(*int_range, binsize = kws.pop('binsize', 0.003))
            
        p = PeakFinder.find_peaks(ms.data.raw, mzmin=mzmin,
                                  savitzky_golay = savitzky_golay,
                                  mzmax=mzmax, thres = thres,
                                  thres_abs = thres_abs)
        
        #p.index.name = 'spec_id'
        p = p.reset_index()
        
        ###!!!Only if len(isotopomers)>1
        if len(isotopomers) > 1:
            
            #theoretical unique peaks for this set of isotopomers
            unique = Discriminator.all_vs_all(isotopomers)
            
            #get all unique peaks
            unique_set = set(
                    np.hstack([mol.M_H.values for mol in unique])
                )
            
            
            remove_ambig = kws.pop('remove_ambig', True)
            print(remove_ambig)
            #match unique peaks of each isotopomer
            found_unique = []
            for un in unique:
                tmp_found = PeakIdentifier\
                            .identify_ms2(p,
                                            un,
                                            limit_ppm=limit_ppm,
                                            remove_ambig=remove_ambig,
                                            crit_col = kws.pop('crit_col', 'y'),
                                            keep = kws.pop('keep', 'max'))
                found_unique.append(tmp_found)
                
                
            #get theoretical set of ambiguous peaks for this set of isotopomers
            #concat all possible peaks
            ambig = pd.concat([i.ms2 for i in isotopomers]).sort_values(('M_H'))
            #get unique set of possible peaks
            ambig_set = set(ambig.M_H.unique())
            #remove unique peaks from all peaks to get putative redundant peaks
            ambig_set = ambig_set - unique_set
            ambig = ambig.set_index('M_H').loc[list(ambig_set)].reset_index()
            
            '''
            #fragment juggling to prevent information loss!
            frags = pd.pivot_table(ambig, index ='M_H', values = 'frags',
                                   aggfunc=lambda x: [i for i in x])\
                      .frags.apply(lambda x: [str(i) for i in x])\
                          .apply(np.unique)\
                              .apply(lambda x: [eval(i) for i in x])
                              
            #get isotopomer groups for ambig peaks
            grouped = Discriminator.groupedPeaks(isotopomers)
            grouped.columns = ['M_H', 'group']
            frags_grouped = pd.merge(frags.reset_index(), grouped,
                                     right_on='M_H', left_on = 'M_H',
                                     how='inner')
            
            n_frags = pd.pivot_table(ambig,
                                     index ='M_H', values = 'n_frags',
                                     aggfunc='sum')
            
            ambig = pd.merge(frags_grouped,
                             n_frags.reset_index(),
                             right_on='M_H', left_on='M_H',
                             how = 'inner')
            '''
            frags = ambig.pivot_table(index='M_H',
                                      values = ['frags', 'id'],
                                      aggfunc = lambda x: [i for i in x])
            
            frags['n_frags'] = ambig.groupby('M_H').frags\
                .apply(lambda x: sum([len(i) for i in x]))
            
            frags['group'] = frags['id'].apply(lambda x:','.join(x))
            
            ambig = frags.reset_index()
            #ambig.to_clipboard()

           
            #match remaining peaks which are redundant
            found_ambig = PeakIdentifier\
                            .identify_ms2(p,
                                            ambig,
                                            limit_ppm=limit_ppm,
                                            remove_ambig=remove_ambig,
                                            crit_col = kws.pop('crit_col', 'y'),
                                            keep = kws.pop('keep', 'max'))
            """               
                            \
                                .drop('id', axis=1)
        
            global fuck
            from copy import deepcopy
            
            fuck = deepcopy(found_ambig)
            print('fuck ', fuck.shape)
            self.fuck = fuck
                                
            frags = pd.pivot_table(found_ambig[['mz_obs', 'frags']],
                                   index = 'mz_obs',
                                   values = 'frags',
                                   aggfunc = lambda x: [i for i in x]).frags.values
            
            # frags = frags.apply(np.unique)
            print(frags)
                            
            ind = found_ambig.mz_obs.drop_duplicates().index #loosing information, could be different fragments from different isotopomers
            found_ambig = found_ambig.loc[ind]
            found_ambig.frags = frags
            

            found_ambig['group'] = pd.merge(found_ambig, grouped,
                                            left_on = 'mz_calc', right_on='M_H').id.values
            
            """
            
            self.found_unique = found_unique
            self.found_ambig = found_ambig
            
        if len(isotopomers)==1:
            
            self.found = PeakIdentifier\
                            .identify_ms2(p,
                                        isotopomers[0].ms2,
                                        limit_ppm=limit_ppm,
                                        remove_ambig=kws.pop('remove_ambig', True),
                                        crit_col = kws.pop('crit_col', 'y'),
                                        keep = kws.pop('keep', 'max'))
        self.peaks = p
        self.msanalyzer = ms
        
    def plot_results(self, ax=None, plot_spectrum=True, **kws):
        
        if ax == None:
            fig, ax = plt.subplots()
        fig = ax.figure
        
        
        if plot_spectrum:
            self.msanalyzer.analysis.plot_spectrum(ax=ax, linewidth = kws.pop('linewidht', 0.9),
                                                   cursor=kws.pop('cursor', True),
                                                   **kws)
        
        if hasattr(self, 'found_ambig'):       
            ax.scatter(self.found_ambig.mz_obs,
                       self.found_ambig.y, s=30,
                       label = 'ambig')
        if hasattr(self, 'found_unique'):
            for i,df in enumerate(self.found_unique,1):
                if not df.empty:
                    ax.scatter(df.mz_obs, df.y,
                               color='C%s' % i,
                               label = df.id.unique()[0],
                               zorder=10, s=100)
                    
        if hasattr(self, 'found'):
            ax.scatter(self.found.mz_obs,
                       self.found.y, s=30,
                       zorder=10,
                       color = 'C1')
                
        return fig, ax