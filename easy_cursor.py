#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 17:44:44 2019

@author: kantundpeterpan
"""

from matplotlib.widgets import Cursor
import matplotlib.pyplot as plt
from PyQt5 import QtGui, QtWidgets

class EasyCursor(Cursor):
    
    def select_integration_range(self):
        
        data_from_input = plt.ginput(n=2, show_clicks = True)
        integration_range = [x for (x,y) in data_from_input]
        print(integration_range)
        
        return integration_range
    
    def integrate_scans(self):
        integration_range = self.select_integration_range()
        
        tmin = integration_range[0]
        tmax = integration_range[1]
        
        self.msanalyzer.mzml_parser.combine_spectra(tmin, tmax)
        
        self.msanalyzer.analysis.plot_spectrum()
        
    def open_pop_menu(self, event):
        if event.button == 2 and event.inaxes == self.msanalyzer.analysis.tic_ax:
            self.pop_menu.popup(self.qcursor.pos())
        
    def __init__(self, ax, useblit=True, color = 'darkgrey', linewidth=1, msanalyzer = None):
        Cursor.__init__(self,ax, useblit=useblit, color = color, linewidth=linewidth)
        
        if msanalyzer != None:
            self.msanalyzer = msanalyzer
        self.qcursor = QtGui.QCursor()
        self.pop_menu = QtWidgets.QMenu()
        
        action = self.pop_menu.addAction('Select integration interval')
        action.triggered.connect(self.integrate_scans)
        