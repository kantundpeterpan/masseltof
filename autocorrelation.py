#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 16:36:45 2020

@author: kantundpeterpan
"""

"""
y = MS2Search()
"""

'''
### Autocorrelation for charge state detection

#### predefined lag
mz_obs = 1213.384280
spec_id = 255963 #e.g. from MS2Search results
no_of_points = 3000

ac = y.msanalyzer.data.raw.loc[spec_id:spec_id+no_of_points,:].reset_index()

lag = -1/2
plus = y.msanalyzer.data.raw.x.searchsorted(mz_obs + lag)
plus_arr = y.msanalyzer.data.raw.iloc[plus:plus+no_of_points]

np.corrcoef(ac.y[:no_of_points], plus_arr.y)

#### autocorrelation function
####http://greenteapress.com/thinkdsp/html/thinkdsp006.html
acn = np.correlate(ac.y, ac.y, mode ='full') #'same' -N/2 to N/2
N = len(acn)
half = acn[N//2:]
lengths = range(N, N//2, -1)
half /= lengths
half /= half[0]
plt.plot(ac.x.iloc[:N//2+1].diff().cumsum().fillna(0), half)
'''
