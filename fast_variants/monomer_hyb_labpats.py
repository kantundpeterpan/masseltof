#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 15:51:27 2020

@author: kantundpeterpan
"""
import pickle as pkl
from itertools import product

def calc_var_mass(*args):
    #print(args)
    var, mol = args[0]
    mass = 0
    for l,res in zip(var, mol):
        mass+=res_dict[labeling_map[l]][res]
        
    return mass

res_dict = pkl.load(open('../res_dict.pkl','rb'))

labeling_map = {
        '+':'13C 15N',
        '-':'12C 14N'
    }

trihyb_labels = [
     'all light',
     'h1 GlcNAc', 
     'h1 MurNAc',
     'h2',
     'h3',
     'all heavy',
     'h2Hex GlcNAc',
     'h2Hex MurNAc',
     #'h3Ala'
    ]

trihyb_don = [
     '--------', #all light
     '+-------', #h1 GlcNAc
     '--+-----', #h1 MurNAc
     '-----+++', #h2
     '--++++++', #h3
     '++++++++', #all heavy
     '+----+++', #h2Hex GlcNAc
     '--+--+++', #h2Hex MurNAc
     #'--+++-++'  #h3Ala
    ]

trihyb_acc = [
    '--------', #all light
    '------+-', #h1 GlcNAc
    '----+---', #h1 MurNAc
    '+++-----', #h2 
    '++++++--', #h3
    '++++++++', #all heavy
    '+++---+-', #h2Hex GlcNAc
    '+++-+---', #h2Hex MurNAc
    #'++-+++--'  #h3Ala
    ]

tethyb_labels = [
     'all light',
     'hAla.1',
     'hAla.2',
     'h1 GlcNAc',
     'h1 MurNAc',
     'h2',
     'h2Ala',
     'all heavy',
     'h2Hex GlcNAc',
     'h2Hex MurNAc',
     'h3Ala.1',
     'h3Ala.2',
     'h3'
    ]

tethyb_don = [
    '---------', #all light
    '-----+---', #hAla.1
    '--------+', #hAla.2
    '+--------', #h1 GlcNAc
    '--+------', #h1 MurNAc
    '-----+++-', #h2
    '-----++++', #h2Ala
    '+++++++++', #all heavy
    '+----+++-', #h2Hex GlcNAc
    '--+--+++-', #h2Hex GlcNAc
    '--+++-+++', #h3Ala.1
    '--++++++-', #h3Ala.2
    '--+++++++', #h3
    ]

tethyb_acc = [
    '---------', #all light
    '---+-----', #hAla.1
    '-+-------', #hAla.2
    '-------+-', #h1 GlcNAc
    '-----+---', #h1 MurNAc
    '+-++-----', #h2
    '++++-----', #h2Ala
    '+++++++++', #all heavy
    '+-++---+-', #h2Hex GlcNAc
    '+-++-+---', #h2Hex MurNAc
    '+++-+++--', #h3Ala.1
    '+-+++++--', #h3Ala.2
    '+++++++--'  #h3
    ]

hybc = [''.join(x) for x in [*product(tethyb_don, tethyb_acc)]]
