#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 16:28:06 2020

@author: kantundpeterpan
"""

#dict with labeling:residue:mass
#all possible combinations of length n

from itertools import product
import pickle as pkl

tri = ['GlcN',
 'Ac',
 'GlcN_Red',
 'Ac',
 'Lac',
 'Ala',
 'Glu',
 'DAP',
 #'Ala',
 #'DAP',
 #'Ala',
 #'Glu',
 #'Ala',
 #'Lac',
 #'GlcN_Red',
 #'Ac',
 #'GlcN',
 #'Ac'
 ]

tetra = ['GlcN',
 'Ac',
 'GlcN_Red',
 'Ac',
 'Lac',
 'Ala',
 'Glu',
 'DAP',
 'Ala',
 #'DAP',
 #'Ala',
 #'Glu',
 #'Ala',
 #'Lac',
 #'GlcN_Red',
 #'Ac',
 #'GlcN',
 #'Ac'
 ]

tritet = ['GlcN',
 'Ac',
 'GlcN_Red',
 'Ac',
 'Lac',
 'Ala',
 'Glu',
 'DAP',
 'DAP',
 'Ala',
 #'Ala',
 'Glu',
 'Ala',
 'Lac',
 'GlcN_Red',
 'Ac',
 'GlcN',
 'Ac'
 ]

tettet = ['GlcN',
 'Ac',
 'GlcN_Red',
 'Ac',
 'Lac',
 'Ala',
 'Glu',
 'DAP',
 'Ala',
 'DAP',
 'Ala',
 'Glu',
 'Ala',
 'Lac',
 'GlcN_Red',
 'Ac',
 'GlcN',
 'Ac'
 ]

labeling_map = {
        '+':'13C 15N',
        '-':'12C 14N'
    }

res_dict = pkl.load(open('../res_dict.pkl','rb'))

tri_labpats = [''.join(bla) for bla in product(('+','-'), repeat=len(tri))]
tet_labpats = [''.join(bla) for bla in product(('+','-'), repeat=len(tetra))]

#n = list(range(len(labpats)))

def calc_var_mass(*args):
    #print(args)
    var, mol = args[0]
    mass = 0
    for l,res in zip(var, mol):
        mass+=res_dict[labeling_map[l]][res]
        
    return mass

ms = [(calc_var_mass([v, tettet])+2*IsoDB.H[1]+IsoDB.O[16]+2*IsoDB.H[1])/2 for v in hybc]
df = pd.DataFrame({'pattern':hybc, 'm2h':ms})
dfnew = df.round(5).pivot_table(index='m2h', values='pattern', aggfunc=lambda x:';'.join(x))
h = dfnew.pattern.apply(lambda x:len(x.split(';')))
fig, ax = plt.subplots()
ax.vlines(h.index, 0, h.values)
        



